<?php

namespace Financer\FilterSlider;
error_reporting( E_ALL );
/**
 * Implements FilterSliderUpgradeCommand command.
 */

/**
 * Class UpgradeCommand
 * @package Financer\FilterSlider
 */
class UpgradeCommand extends \WP_CLI_Command {
	/**
	 * @var bool
	 */
	private $_upgraded = false;
	/**
	 * @var null
	 */
	private $_version = null;
	/**
	 * @var int
	 */
	private $_blog = 0;

	/**
	 *
	 */
	public static function register() {
		\WP_CLI::add_command( 'finance_filter_slider_upgrade', get_called_class() );
	}

	/**
	 * Runs the upgrade process for each site for the Filter Slider Plugin
	 *
	 * @synopsis [--blog_id=<blog_id>]
	 * @param    $args
	 * @param    $assoc_args
	 */
	function __invoke( $args, $assoc_args ) {
		global $wpdb;
		$this->_blog = ! empty( $assoc_args['blog_id'] ) ? $assoc_args['blog_id'] : null;
		if ( empty( $this->_blog ) ) {
			$blogs = $wpdb->get_results( "SELECT blog_id FROM {$wpdb->blogs} WHERE site_id = '{$wpdb->siteid}' AND spam = '0' AND deleted = '0' AND archived = '0' ORDER BY registered DESC", ARRAY_A );
			foreach ( (array) $blogs as $details ) {
				\WP_CLI::log( \WP_CLI::launch_self( 'finance_filter_slider_upgrade', [], [ 'blog_id' => $details['blog_id'] ], false, true )->stdout );
			}
		} else {
			switch_to_blog( $this->_blog );
			add_filter(
				'pods_error_die',
				function () {
					return true;
				}
			);
			$this->_version = get_option( 'financer_filter_slider_version', '0' );
			$this->_import_packages();
			if ( ! empty( $this->_version ) ) {
				$this->_upgrade_1_0();
				$this->_upgrade_1_1();
				$this->_upgrade_1_2();
				$this->_upgrade_2_6_0();
			}
			update_option( 'financer_filter_slider_version', Plugin::VERSION );
			$blog_info = get_blog_details( $this->_blog );
			if ( $this->_upgraded ) {
				\WP_CLI::success( sprintf( 'Upgraded site %s%s from %s to %s', $blog_info->domain, $blog_info->path, $this->_version, Plugin::VERSION ) );
			} else {
				\WP_CLI::log( sprintf( 'No upgrade required for site %s%s', $blog_info->domain, $blog_info->path, $this->_version, Plugin::VERSION ) );

			}
			restore_current_blog();
		}
	}

	/**
	 *
	 */
	private function _import_packages() {
		pods_init()->setup( $this->_blog );
		pods_init()->load_components();
		pods_components()->get_components();
		pods_components()->load();
		if ( ! class_exists( 'Pods_Migrate_Packages' ) ) {
			$_GET['toggle'] = 1;
			pods_components()->toggle( 'migrate-packages' );
			pods_components()->load();
		}
		if ( ! class_exists( 'Pods_Advanced_Relationships' ) ) {
			$_GET['toggle'] = 1;
			pods_components()->toggle( 'advanced-relationships' );
			pods_components()->load();
		}
		pods_api()->import_package( file_get_contents( Plugin::GetDir( 'pods.json' ) ) );
		pods_api()->save_field( [ 'pod' => 'loan_dataset', 'name' => 'company_parent', 'sister_id' => pods_api()->load_field( [ 'pod' => 'company_single', 'name' => 'loan_datasets' ] )['id'] ] );
		pods_api()->cache_flush_pods();
		if ( defined( 'PODS_PRELOAD_CONFIG_AFTER_FLUSH' ) && PODS_PRELOAD_CONFIG_AFTER_FLUSH ) {
			pods_api()->load_pods();
		}
	}

	/**
	 *
	 */
	private function _upgrade_1_0() {
		if ( version_compare( '1.0.0', $this->_version ) == 1 ) {
			if ( ! is_plugin_active( 'advanced-custom-fields/acf.php' ) ) {
				\WP_CLI::error( 'Advanced Custom Fields must be enabled to upgrade' );
				wp_die();
			}
			if ( ! function_exists( 'get_field' ) ) {
				include_once WP_PLUGIN_DIR . '/advanced-custom-fields/acf.php';
			}
			$filters = pods( 'filter_single', [ 'limit' => - 1 ] );
			while ( $filters->fetch() ) {
				$meta = get_post_meta( $filters->id() );
				$filters->save( 'company_parent', self::val( $meta['company_parent'] ) );
				$filters->save( 'old_new_client', self::val( $meta['old_new_client'] ) );
				$filters->save( 'summ', self::num_val( $meta['summ'] ) );
				$filters->save( 'period', self::num_val( $meta['period'] ) );
				$filters->save( 'percent', self::val( $meta['percent'] ) );
				$filters->save( 'looses', self::num_val( $meta['looses'] ) );
				$filters->save( 'total', self::num_val( $meta['total'] ) );
			}
			$creditcards = pods( 'creditcard', [ 'limit' => - 1 ] );
			while ( $creditcards->fetch() ) {
				$meta = get_post_meta( $creditcards->id() );
				$creditcards->save( 'summ', self::num_val( $meta['summ'] ) );
				$creditcards->save( 'period', self::num_val( $meta['period'] ) );
				$creditcards->save( 'percent', self::val( $meta['percent'] ) );
				$creditcards->save( 'looses', self::num_val( $meta['looses'] ) );
				$creditcards->save( 'total', self::num_val( $meta['total'] ) );
				$creditcards->save( 'atm_fee', self::num_val( $meta['atm_fee'] ) );
				$creditcards->save( 'card_type', self::val( $meta['card_type'] ) );
				$creditcards->save( 'travel_insurance', self::bool_val( $meta['travel_insurance'] ) );
			}
			$companies        = pods( 'company_single', [ 'limit' => - 1 ] );
			$banks            = [];
			$credit_companies = [];
			while ( $companies->fetch() ) {
				$meta = get_post_meta( $companies->id() );
				if ( array_key_exists( 'credit_check', $meta ) ) {
					$credit_company = self::val( $meta['credit_check'] );
					if ( ! array_key_exists( $credit_company, $credit_companies ) ) {
						$credit_company_item = get_page_by_title( $credit_company, OBJECT, 'credit_check_company' );
						if ( null == $credit_company_item ) {
							$credit_companies[ $credit_company ] = pods( 'credit_check_company' )->add(
								[
									'post_title'  => $credit_company,
									'post_status' => 'publish',
								]
							);
						} else {
							$credit_companies[ $credit_company ] = $credit_company_item->ID;
						}
					}
				}
				$bank_list = [];
				if ( array_key_exists( 'banks', $meta ) ) {
					foreach ( explode( ',', self::val( $meta['banks'] ) ) as $bank ) {
						$bank = trim( $bank );
						if ( empty( $bank ) ) {
							continue;
						}
						$bank_item = get_page_by_title( $bank, OBJECT, 'bank' );
						if ( false == array_key_exists( $bank, $banks ) ) {
							if ( null == $bank_item ) {
								$banks[ $bank ] = pods( 'bank' )->add(
									[
										'post_title'  => $bank,
										'post_status' => 'publish',
									]
								);
							} else {
								$banks[ $bank ] = $bank_item->ID;
							}
						}
						$bank_list[] = $banks[ $bank ];
					}
				}
				$companies->save( 'credit_check', empty( $credit_company ) ? 0 : $credit_companies[ $credit_company ] );
				$companies->save( 'bad_history', self::bool_val( $meta['bad_history'] ) );
				$companies->save( 'banks', $bank_list );
				$companies->save( 'national_bank', self::bool_val( $meta['national_bank'] ) );
				$companies->save( 'national_phone', self::bool_val( $meta['national_phone'] ) );
				$companies->save( 'minimum_inkomst', self::val( \get_field( 'minimum_inkomst', $companies->id(), false ) ) );
				$companies->save( 'helgutbetalning', self::val( \get_field( 'helgutbetalning', $companies->id(), false ) ) );
				$companies->save( 'foretag', self::val( \get_field( 'foretag', $companies->id(), false ) ) );
				$companies->save( 'telefon', self::val( \get_field( 'telefon', $companies->id(), false ) ) );
				$companies->save( 'hemsida', self::val( \get_field( 'hemsida', $companies->id(), false ) ) );
				$companies->save( 'fornya_lan', self::val( \get_field( 'fornya_lan', $companies->id(), false ) ) );
				$companies->save( 'minalder', self::val( \get_field( 'minalder', $companies->id(), false ) ) );
				$companies->save( 'mandag_fredag', self::val( \get_field( 'mandag-fredag', $companies->id(), false ) ) );
				$companies->save( 'lordag', self::val( \get_field( 'lordag', $companies->id(), false ) ) );
				$companies->save( 'sondag', self::val( \get_field( 'sondag', $companies->id(), false ) ) );
				$companies->save( 'favorite', self::bool_val( $meta['favorite'] ) );
				$companies->save( 'ej_partner', self::val( \get_field( 'ej_partner', $companies->id(), false ) ) );
				$companies->save( 'payment_times', self::val( \get_field( 'payment_times', $companies->id(), false ) ) );
				$companies->save( 'email', self::val( \get_field( 'email', $companies->id(), false ) ) );
				$companies->save( 'loan_broker', self::bool_val( $meta['loan_broker'] ) );
			}
			$this->_upgraded = true;
		}
	}

	/**
	 * @param $data
	 *
	 * @return mixed
	 */
	private static function val( $data ) {
		return is_array( $data ) ? $data[0] : $data;
	}

	/**
	 * @param $data
	 *
	 * @return mixed
	 */
	private static function num_val( $data ) {
		return str_replace( ' ', '', self::val( $data ) );
	}

	/**
	 * @param $data
	 *
	 * @return bool
	 */
	private static function bool_val( $data ) {
		return self::val( $data ) == 'on' || self::val( $data ) == 1;
	}

	private function _upgrade_1_1() {
		if ( version_compare( '1.1.0', $this->_version ) == 1 ) {
			if ( ! is_plugin_active( 'advanced-custom-fields/acf.php' ) ) {
				\WP_CLI::error( 'Advanced Custom Fields must be enabled to upgrade' );
				wp_die();
			}
			if ( ! function_exists( 'get_field' ) ) {
				include_once WP_PLUGIN_DIR . '/advanced-custom-fields/acf.php';
			}
			$banks = pods( 'bank', [ 'limit' => - 1 ] );
			while ( $banks->fetch() ) {
				$meta = get_post_meta( $banks->id() );
				$banks->save( 'affiliate_relationship', self::bool_val( \get_field( 'sponsorbank', $banks->id(), false ) ) );
				$banks->save( 'url', self::val( $meta['url'] ) );
				$banks->save( 'email', self::val( \get_field( 'epost', $banks->id(), false ) ) );
				$banks->save( 'address', self::val( \get_field( 'adress', $banks->id(), false ) ) );
				$banks->save( 'phone', self::val( \get_field( 'telefon', $banks->id(), false ) ) );
				$banks->save( 'bank_id', self::bool_val( \get_field( 'bankid', $banks->id(), false ) ) );
				$banks->save( 'company_name', self::val( \get_field( 'foretag', $banks->id(), false ) ) );
				$banks->save( 'opening_hours', self::val( \get_field( 'oppetider', $banks->id(), false ) ) );
				$banks->save( 'governmental_guarantee', self::bool_val( \get_field( 'gov_guarantee', $banks->id(), false ) ) );
			}
			$savings_accounts = pods( 'savings_account', [ 'limit' => - 1 ] );
			while ( $savings_accounts->fetch() ) {
				$meta = get_post_meta( $savings_accounts->id() );
				$savings_accounts->save( 'bank', self::val( $meta['bank'] ) );
				$savings_accounts->save( 'min_save_amount', self::num_val( $meta['min_save_amount'] ) );
				$savings_accounts->save( 'max_save_amount', self::num_val( $meta['max_save_amount'] ) );
				$savings_accounts->save( 'min_save_time', self::num_val( $meta['min_save_time'] ) );
				$savings_accounts->save( 'free_withdrawals', self::val( $meta['free_withdrawals'] ) );
				$savings_accounts->save( 'interest_rate', self::val( $meta['int_rate'] ) );
				$savings_accounts->save( 'floating_interest_rate', self::bool_val( $meta['floating_int'] ) );
			}
			$this->_upgraded = true;
		}
	}

	private function _upgrade_1_2() {
		if ( version_compare( '1.2.0', $this->_version ) == 1 ) {
			pods_api()->save_field(
				[
					'pod'      => 'savings_account',
					'name'     => 'bank',
					'pick_val' => 'bank',
				]
			);
			pods_api()->save_field(
				[
					'pod'      => 'company_single',
					'name'     => 'banks',
					'pick_val' => 'bank',
				]
			);
			$this->_flush_pods();
			$savings_account_bank_list = [];
			$company_single_bank_list  = [];
			$companies                 = pods(
				'company_single', [
					'limit' => - 1,
				]
			);
			$savings_accounts          = pods(
				'savings_account', [
					'limit' => - 1,
					'where' => [
						'post_status' => [ 'publish', 'private', 'draft' ],
					],
				]
			);
			while ( $savings_accounts->fetch() ) {
				$savings_account_bank_list[ $savings_accounts->field( 'bank' )['ID'] ][] = $savings_accounts->id();
			}
			while ( $companies->fetch() ) {
				if ( $companies->field( 'banks' ) ) {
					foreach ( $companies->field( 'banks' ) as $bank ) {
						$company_single_bank_list[ $companies->id() ][] = $bank['ID'];
					}
				}
			}
			pods_api()->save_field(
				[
					'pod'      => 'savings_account',
					'name'     => 'bank',
					'pick_val' => 'company_single',
				]
			);
			pods_api()->save_field(
				[
					'pod'      => 'company_single',
					'name'     => 'banks',
					'pick_val' => 'company_single',
				]
			);
			$banks = pods(
				'bank', [
					'limit' => - 1,
					'where' => [
						'post_status' => [ 'publish', 'private', 'draft' ],
					],
				]
			);
			while ( $banks->fetch() ) {
				$company                = $companies->find(
					[
						'where' =>
							[
								'post_name' => $banks->field( 'post_name' ),
							],
					]
				);
				$bank_id                = $banks->field( 'bank_id' );
				$governmental_guarantee = $banks->field( 'governmental_guarantee' );
				if ( is_array( $bank_id ) ) {
					if ( empty( $bank_id ) ) {
						$bank_id = 0;
					} else {
						$bank_id = $bank_id[0];
					}
				}
				if ( is_array( $governmental_guarantee ) ) {
					if ( empty( $governmental_guarantee ) ) {
						$governmental_guarantee = 0;
					} else {
						$governmental_guarantee = $bank_id[0];
					}
				}
				if ( strlen( $bank_id ) == 0 ) {
					$bank_id = 0;
				}
				if ( strlen( $governmental_guarantee ) == 0 ) {
					$governmental_guarantee = 0;
				}
				$bank_id = (int) $bank_id;
				if ( ! $company || $company->total() == 0 ) {
					$id = $companies->add(
						[
							'post_title'             => $banks->field( 'post_title' ),
							'post_name'              => $banks->field( 'post_name' ),
							'post_status'            => $banks->field( 'post_status' ),
							'savings_url'            => $banks->field( 'url' ),
							'email'                  => $banks->field( 'email' ),
							'address'                => $banks->field( 'adress' ),
							'telefon'                => $banks->field( 'phone' ),
							'bank_id'                => $bank_id,
							'foretag'                => $banks->field( 'company_name' ),
							'bank_opening_hours'     => $banks->field( 'opening_hours' ),
							'governmental_guarantee' => $governmental_guarantee,
							'company_type'           => 'bank',
						]
					);
				} else {
					$company->save(
						[
							'savings_url'            => $banks->field( 'url' ),
							'bank_id'                => $banks->field( 'bank_id' ),
							'bank_opening_hours'     => $banks->field( 'opening_hours' ),
							'governmental_guarantee' => $governmental_guarantee,
						]
					);
					$company->add_to( 'company_type', 'bank' );
					$id = $company->id();
				}

				foreach ( $company_single_bank_list as $company => &$bank_list ) {
					$pos = array_search( $banks->id(), $bank_list );
					if ( false !== $pos ) {
						$bank_list[ $pos ] = $id;
					}
				}
				if ( isset( $savings_account_bank_list[ $banks->id() ] ) ) {
					foreach ( $savings_account_bank_list[ $banks->id() ] as $savings_account ) {
						pods( 'savings_account', $savings_account )->save( 'bank', $id );
					}
				}
				$banks->delete();
			}
			$companies = pods(
				'company_single', [
					'limit' => - 1,
				]
			);
			while ( $companies->fetch() ) {
				if ( isset( $company_single_bank_list[ $companies->id() ] ) ) {
					$companies->save( 'banks', $company_single_bank_list[ $companies->id() ] );
				}
			}

			$companies = pods(
				'company_single', [
					'limit' => - 1,
					'where' =>
						[
							'post_status' => [ 'publish', 'private', 'draft' ],
							[
								'key'     => 'company_type',
								'compare' => 'NOT EXISTS',
							]
						],
				]
			);
			while ( $companies->fetch() ) {
				$companies->add_to( 'company_type', 'loan_company' );
			}
			pods_api()->delete_pod( 'bank' );
			pods_api()->delete_field(
				[
					'name' => 'bank',
					'pod'  => 'company_single',
				]
			);
			pods_api()->delete_field(
				[
					'name' => 'total',
					'pod'  => 'filter_single',
				]
			);
			$this->_flush_pods();
			$this->_upgraded = true;
		}
	}

	private function _flush_pods() {
		pods_api()->cache_flush_pods();
		if ( defined( 'PODS_PRELOAD_CONFIG_AFTER_FLUSH' ) && PODS_PRELOAD_CONFIG_AFTER_FLUSH ) {
			pods_api()->load_pods();
		}
	}

	private function _upgrade_2_6_0() {
		if ( version_compare( '2.6.0', $this->_version ) == 1 ) {
			$this->_import_packages();
			$pods                  = pods_api()->load_pods();
			$terms                 = [];
			$terms['loan_company'] = wp_insert_term( 'Loan Company', 'company_type', [
				'slug' => 'loan_company'
			] )['term_id'];
			$terms['bank']         = wp_insert_term( 'Bank', 'company_type' )['term_id'];
			foreach ( $pods as $podEntity ) {
				if ( 'post_type' !== $podEntity['type'] || 'page' === $podEntity['object'] ) {
					continue;
				}
				$podEntity['storage'] = 'table';
				pods_api()->save_pod( $podEntity );
				$pod    = pods( $podEntity['name'] );
				$fields = [];
				foreach ( $podEntity['fields'] as $field ) {
					if ( 'pick' !== $field['type'] || ( 'pick' === $field['type'] && 'custom-simple' === $field['pick_object'] ) ) {
						$fields[ $field['name'] ] = $field['name'];
					}
				}
				$query = new \WP_Query( [
					'post_type'      => $podEntity['object'],
					'post_status'    => 'any',
					'posts_per_page' => - 1
				] );
				while ( $query->have_posts() ) {
					$query->the_post();
					$meta = get_post_meta( get_the_ID() );
					$data = array_intersect_key( $meta, $fields );
					$data = array_map( function ( $item ) {
						return $item[0];
					}, $data );
					$pod->save( $data, null, get_the_ID() );
				}
			}
			$query = new \WP_Query( [
				'post_type'      => 'company_single',
				'post_status'    => 'any',
				'posts_per_page' => - 1
			] );
			while ( $query->have_posts() ) {
				$query->the_post();
				$types = get_post_meta( get_the_ID(), 'company_type' );
				$types = array_map( function ( $item ) use ( &$terms ) {
					return isset( $terms[ $item ] ) ? $terms[ $item ] : false;
				}, $types );
				$types = array_filter( $types );
				wp_set_post_terms( get_the_ID(), $types, 'company_type' );
			}
			pods_api()->delete_field( [ 'pod' => 'company_single', 'name' => 'company_type' ] );
			$this->_upgraded = true;
		}
	}
}
