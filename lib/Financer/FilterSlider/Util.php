<?php
namespace Financer\FilterSlider;


/**
 * Class Util
 * @package Financer\FilterSlider
 */
/**
 * Class Util
 * @package Financer\FilterSlider
 */
class Util {
	/**
	 * @param $period
	 *
	 * @return string
	 */
	public static function getPeriod( int $period ): string {
		if ( 0 === $period % 365 ) {
			$years           = ( $period / 365 );
			$interval_format = 'P' . floor( $years ) . 'Y';
		} else if ( 0 === $period % 360 ) {
			$years           = ( $period / 360 );
			$interval_format = 'P' . floor( $years ) . 'Y';
		} else if ( 0 === $period % 30 ) {
			$months          = ( $period / 30 );
			$interval_format = 'P' . floor( $months ) . 'M';
		} else {
			$interval_format = 'P' . floor( $period ) . 'D';
		}
		if ( is_float( $period ) ) {
			$interval_format .= 'T' . floor( ( $period - ( floor( $period ) ) ) * 24 ) . 'H';
		}
		$interval    = ( new \DateTime() )->diff( ( new \DateTime() )->add( new \DateInterval( $interval_format ) ) );
		$period_list = [];
		if ( 28 <= $interval->d ) {
			$interval->m ++;
			$interval->d = 0;
		}
		if ( 12 == $interval->m ) {
			$interval->y ++;
			$interval->m = 0;
		}
		if ( 0 < $interval->y ) {
			$period_list[] = sprintf( $interval->format( '%y %%s' ), 1 < $interval->y ? __( 'Years', 'fs' ) : __( 'Year', 'fs' ) );
		}
		if ( 0 < $interval->m ) {
			$period_list[] = sprintf( $interval->format( '%m %%s' ), 1 < $interval->m ? __( 'Months', 'fs' ) : __( 'Month', 'fs' ) );
		}
		if ( 0 < $interval->d && 0 == $interval->y && 0 == $interval->m ) {
			$period_list[] = sprintf( $interval->format( '%d %%s' ), 1 < $interval->d ? __( 'Days', 'fs' ) : __( 'Day', 'fs' ) );
		}

		return pods_serial_comma( $period_list, [ 'and' => ' ' . __( 'and', 'fs' ) . ' ' ] );
	}

	/**
	 * @param $amount
	 *
	 * @return string
	 */
	public static function moneyFormat( $amount ): string {
		$amount = self::numberFormat( $amount, 2 );
		if ( strpos( $amount, '.' ) !== false ) {
			return number_format_i18n( $amount, 2 );
		} else {
			return number_format_i18n( $amount );
		}
	}

	/**
	 * @param     $number
	 * @param int $decimals
	 *
	 * @return string
	 */
	public static function numberFormat( $number, $decimals = 2 ): string {
		$number       = round( (float) $number, $decimals );
		$number_parts = explode( '.', $number );
		if ( 1 < count( $number_parts ) ) {
			if ( 0 == absint( $number_parts[1] ) ) {
				$number = absint( $number );
			}
		}

		return (string) $number;
	}

}
