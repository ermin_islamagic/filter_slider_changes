<?php


namespace Financer\FilterSlider\Table;


use Financer\FilterSlider\Abstracts\Slider;
use Financer\FilterSlider\Abstracts\Table;
use Financer\FilterSlider\Interfaces\TableInterface;
use Financer\FilterSlider\Surface\Data;
use Financer\FilterSlider\Surface\Row;
use Financer\FilterSlider\Surface\Surface;
use Financer\FilterSlider\Util;

/**
 * Class TopCompaniesTable
 * @package Financer\FilterSlider\Table
 */
class TopCompaniesTable extends Table implements TableInterface {

	/**
	 * @param null|\Pods $pod
	 *
	 * @param Slider     $slider
	 *
	 * @internal param int $year
	 *
	 * @internal param Slider $slider
	 *
	 * @internal param null $postType
	 *
	 * @internal param array $query
	 */
	public static function build( \Pods $pod, Slider $slider = null ) {
		$query = $pod->data();
		if ( ! $query ) {
			$query = [];
		}
		$generalSettings = pods( 'general_settings' );
		$sliderSetting = pods( 'slider_settings' );
		$remove_apr = $sliderSetting->field( 'remove_apr' );
		$remove_style = "";
		if($remove_apr==1){
			$remove_style = "display:none;";
		}

		$table = new Surface( [ 'class' => 'table table-striped' ] );
		$table->setHead( new Row( [
			new Data( __( 'Loan company', 'fs' ), [ 'title' => __( 'Logo for company', 'fs' ), 'class' => 'vit' ] ),
			new Data( __( 'Loan amount', 'fs' ), [
				'title' => __( 'Max standard credit', 'fs' ),
				'class' => 'sliderm',
			] ),
			new Data( __( 'Bad History', 'fs' ), [
				'title' => __( 'Lenders accepting bad credit history', 'fs' ),
				'class' => 'sliderm',
			] ),
			new Data( __( 'Weekend Payout', 'fs' ), [ 'title' => __( 'Weekend Payout', 'fs' ), 'class' => 'sliderm', 'style'=>'sliderm' ] ),
			new Data( __( 'Min. age', 'fs' ), [ 'class' => 'sliderm', 'title' => __( 'The minimum age you need to borrow money', 'fs' ) ] ),
			new Data( __( 'Apply', 'fs' ), [ 'title' => __( 'Apply for a loan below', 'fs' ) ] ),
		] ) );
		if ( count( $query ) > 0 ) {
			foreach ( $query as $pos => $result ) {

				if(! empty($result->specific_affiliate_url)){
					$url_link = $result->specific_affiliate_url;
				}else{
					$url_link = user_trailingslashit( get_permalink( $result->ID ) . 'redirect' );
				}

				$func = function ( $meta_value, $post_id, $meta_key ) use ( $result ) {
					if ( $post_id == $result->ID ) {
						if ( 'crfp-average-rating' == $meta_key ) {
							$meta_value = ( intval( ( $result->rating * 2 ) + 0.5 ) / 2 );
						}
					}

					return $meta_value;
				};
				add_filter( 'get_post_metadata', $func, 10, 3 );
				$table->addRow( new Row( [
					// Logo

					new Data( '<i class="mega-icon-eraser report"><a href="#" title="' . __( 'Wrong data? Report this item', 'fs' ) . '">&nbsp;</a></i>' . ( ( null !== $slider && ! $slider->isPdf() ) ? ( !$result->representative_example && !$output_loan_tags ? '<i title="' . __( 'More information', 'fs' ) . '" class="toggle-details fa fa-plus" ></i>' : '') : '' ) . '<a href="' . get_permalink( $result->ID ) . '">' . '<img title="' . $result->title . '" src="' . $pod->field( 'logo._src' ) . '" />' . '</a>' .
					          self::showStars( $result->ID ),

					          ['class' => 'loan-company '] ),
					// Loan Amounts
					new Data( '<span class="mobile-only">' . __( 'Loan amount:', 'fs' ) . '</span>'. Util::moneyFormat( $result->amount_range_minimum ) . ' - ' . Util::moneyFormat( $result->amount_range_maximum ) . ' ' . __( 'usd', 'fs' ) ),
					// 
					new Data( '<span class="mobile-only">' . __( 'Bad credit history', 'fs' ) . '</span><p class="' . ( $result->bad_history ? 'true' : 'false' ) . '"></p>', [ 'class' => 'sliderm' ] ),

					// 
					new Data( '<span class="mobile-only">' . __( 'Weekend Payout', 'fs' ) . '</span><p class="' . ( $result->weekend_payout ? 'true' : 'false' ) . '"></p>', [ 'class' => 'sliderm' ] ),
					
					
					//
					new Data( '<span class="mobile-only">' . __( 'Minimum age:', 'fs' ) . '</span> ' . $result->minalder, [ 'class' => 'minimum-age' ] ),

					new Data( '<a href="' . $url_link . '" class="button small applyYellow" target="_blank" rel="nofollow"> ' . __( 'Application', 'fs' ) . ' </a><a href="' . get_permalink( $result->ID ) . '" class="applyNow">' . __( 'Read more', 'fs' ) . '</a>' . ($generalSettings->field('show_sponsored_text')? '<div class="sponsored">' . __( 'Sponsored', 'fs' ) . '</div>': ''), [ 'class' => 'loan-apply' ] ),
					
				], [ 'data-id' => $result->ID, 'class' => 'sort-item ' . ( $pos % 2 ? ' even' : ' odd' ) ] ) );
				remove_filter( 'get_post_metadata', $func );
				$pod->fetch();
			}
		} else {
			$table->addRow( new Row( [ new Data( __( 'No companies found', 'fs' ), [ 'colspan' => 100 ] ) ] ) );
		}
		echo $table->render();
	}
}