<?php

namespace Financer\FilterSlider\Table;


use Financer\FilterSlider\Abstracts\Slider;
use Financer\FilterSlider\Abstracts\Table;
use Financer\FilterSlider\Interfaces\TableInterface;
use Financer\FilterSlider\Util;
use Financer\FilterSlider\RepresentativeExampleUtil;
use Financer\FilterSlider\Surface\Data;
use Financer\FilterSlider\Surface\Row;
use Financer\FilterSlider\Surface\Surface;

/**
 * Class SliderLoanTable
 * @package Financer\FilterSlider\Table
 */
class SliderLoanTable extends Table implements TableInterface {
	/**
	 * @param null|\Pods  $pod
	 *
	 * @param Slider|null $slider
	 *
	 * @return void
	 *
	 */
	public static function build( \Pods $pod, Slider $slider = null ) {
		$query = $pod->data();

		if ( ! $query ) {
			$query = [];
		}
		
		
		$generalSettings = pods( 'general_settings' );
		$sliderSetting = pods( 'slider_settings' );
		$remove_apr = $sliderSetting->field( 'remove_apr' );
		$remove_style = "";
		if($remove_apr==1){
			$remove_style = "display:none;";
		}

		$table = new Surface( [ 'class' => 'table table-striped' ] );
		
		$table->setHead( new Row( [
			new Data( __( 'Loan company', 'fs' ), [ 'title' => __( 'The lender with the loan offer', 'fs' ), 'class' => 'vit' ] ),
			new Data( __( 'Loan amount', 'fs' ), [
				'title' => __( 'The searched loan amount. Lenders may lend other amounts', 'fs' ),
				'class' => 'sliderm',
			] ),
			new Data( __( 'Loan period', 'fs' ), [
				'title' => __( 'The loan period you are searching for. The lender may lend in shorter or longer periods', 'fs' ),
				'class' => 'sliderm',
			] ),
			new Data( __( 'APR', 'fs' ), [ 'title' => __( 'The annual interest rate (not effective rate)', 'fs' ), 'class' => 'sliderm', 'style'=>$remove_style ] ),
			new Data( __( 'Monthly Payback *', 'fs' ), [ 'title' => __( 'Estimated monthly payback, based upon lowest rates', 'fs' ), 'class' => 'sliderm' ] ),
			new Data( __( 'Total cost *', 'fs' ), [
				'title' => __( 'This is the estimated lowest total cost of the loan, based upon lowest rates. Be aware that the total cost may vary and be set individually sometimes', 'fs' ),
				'class' => 'sliderm sorted',
			] ),
			new Data( __( 'Loan', 'fs' ), [ 'title' => __( 'Apply for a loan below', 'fs' ), 'class' => 'sliderd' ] ),
			new Data( __( 'Apply', 'fs' ), [ 'title' => __( 'Apply for a loan below', 'fs' ) ] ),
		] ) );
		if ( count( $query ) > 0 ) {
			foreach ( $query as $pos => $result ) {

				$date = new \DateTime();
				$date->add( new \DateInterval( 'P' . $slider->getPeriod() . 'D' ) );
				if ( 'new' == $result->loan_restriction ) {
					$restriction = '<span class="newSpan">' . __( 'New', 'fs' ) . '</span>';
				} else if ( 'old' == $result->loan_restriction ) {
					$restriction = '<span class="oldSpan">' . __( 'Old', 'fs' ) . '</span>';
				} else {
					$restriction = '<span class="allSpan">' . __( 'All', 'fs' ) . '</span>';
				}
				$url_link = user_trailingslashit( get_permalink( $result->ID ) . 'redirect' ) . "?b=s";

				if(! empty($result->specific_affiliate_url)){
					$url_link = user_trailingslashit( get_permalink( $result->ID ) ) . 'redirect/?id=' . $result->pid . "&b=s";
				}

								
				/*loan tags*/
				$term_list = wp_get_post_terms($result->pid, 'loan_tags', array("fields" => "all"));
				 $output_loan_tags = '';
				 $output_loan_slug = '';

				$sliderSetting = pods( 'slider_settings' );
				$display_tags = $sliderSetting->field( 'display_tags_in_slider_results' );
				if($display_tags==0){
				 foreach($term_list as $loan_tag){
				    $output_loan_tags .= '<div class="data-tag ' . $loan_tag->slug . '" style="background:' . $loan_tag->description . ';">' . $loan_tag->name . '</div> ';
				    $output_loan_slug .= ' '. $loan_tag->slug;
				  }
				}
				  //

				  /*representative example 
				  * if loan dataset representative example is not empty
				  * display loan dataset rep examples
				  * else if slider settings rep example is not empty
				  * show slider settings rep example
				  * else display nothing 
				  */
				  $repexample = "";

				  if(!empty($sliderSetting->field( 'representative_example' ))){

				  	$repexample = do_shortcode(RepresentativeExampleUtil::RepresentativeExample($sliderSetting->field( 'representative_example' ), $result, $slider));
				  }

				  		  

				  /**/
				  
				$table->addRow( new Row( [
					// Logo

					new Data( '' . ( ( null !== $slider && ! $slider->isPdf() ) || false === (bool) $result->ej_partner ? ( !$repexample && !$output_loan_tags ? '<i title="' . __( 'More information', 'fs' ) . '" class="toggle-details fa fa-plus" >' . __( 'More information', 'fs' ) . '</i>' : '') : '' ) . '<a href="' . get_permalink( $result->ID ) . '">' . '<img title="' . $result->title . '" src="' . $pod->field( 'logo._src' ) . '" />' . '</a>' . ( $repexample || $output_loan_tags ? '<div class="' . $output_loan_slug . '"></div>' : '') .
					          self::showStars( $result->ID ) . ' <span class="totalReviews"><a href="' . get_permalink( $result->ID ) . '#read-reviews">' . __( 'Read', 'fs' ) . '&nbsp;' . $result->total_reviews . ' ' . __( 'reviews.', 'fs' ) . '</a></span>' . '<span class="sort-company" style="display: none;">' . preg_replace('/\PL/u', '', lcfirst($result->title)) . '</span><span class="sort-rating" style="display: none;">' . $result->rating . '</span>',

					          ['class' => 'loan-company ' .
						           ( $result->favorite ? 'vit premium' : 'vit' ) . ( $result->ej_partner ? ' np' : '' )
					] ),
					// Loan Amounts
					new Data( '<span class="mobile-only">' . __( 'Loan amount:', 'fs' ) . '</span> ' . Util::moneyFormat( $slider->getAmount() ) . ' ' . __( 'usd', 'fs' ) . '<br>' . ' ' . $restriction, [ 'class' => 'loan-amount' ] ),
					// Loan Period
					new Data( '<span class="mobile-only">' . __( 'Loan period:', 'fs' ) . '</span> ' . Util::getPeriod( $slider->getPeriod() ), [ 'class' => 'loan-period' ] ),
					// Loan APR
					new Data( '<span class="mobile-only">' . __( 'Nominal APR:', 'fs' ) . '</span> ' . ( $result->personal_loans ? __( 'from', 'fs' ) . ' ' : '' ) . '<span class="sort-interest">' . $result->interest_rate . '</span>&nbsp;% ' . ( $result->highest_annual_interest_rate!=0 ? __( 'to', 'fs' ) . ' ' .  Util::numberFormat( $result->highest_annual_interest_rate ) . ' %' : '' ), [ 'class' => 'loan-apr', 'style'=>$remove_style ] ),
					// Loan Fees
					new Data( ($sliderSetting->field( 'remove_apr' )==1 ? '<span class="sort-interest"></span>' : '') . '<span class="mobile-only">' . __( 'Monthly payback:', 'fs' ) . '</span> ' . Util::moneyFormat( $result->total_monthly_payback ) . ' ' . __( 'usd', 'fs' ), [ 'class' => 'loan-monthly-payback' ] ),
					// Loan Total Cost
					new Data( '<span class="mobile-only">' . __( 'Total cost from:', 'fs' ) . '</span><span class="sort-total" style="display: none;">' . $result->total_cost . '</span> ' . Util::moneyFormat( $result->total_cost ) . ' ' . __( 'usd', 'fs' ) . ( 0 == $result->total_cost ? ' <span class="greenSpan">' . __( 'Free loan', 'fs' ) . ' </span>' : '' ), [ 'class' => 'loan-total fet' . ( 0 == $result->total_cost ? ' green' : '' ) ] ),
					// Loan Apply				
					new Data( '<a href="' . $url_link . '" class="button small applyYellow" target="_blank" rel="nofollow"> ' . __( 'Application', 'fs' ) . ' </a>'  . ($generalSettings->field('show_sponsored_text')? '<div class="sponsored">' . __( 'Sponsored', 'fs' ) . '</div>': '') . '<a href="' . get_permalink( $result->ID ) . '" class="applyNow">' . __( 'Read more', 'fs' ) . '</a>', [ 'class' => 'loan-apply' ] ),
					//

					//tags
					
				
					//
					
				], [ 'data-id' => $result->ID, 'class' => 'sort-item ' . ( $pos % 2 ? ' even' : ' odd' ) . ( $result->ej_partner ? ' greyed' : '' ) . ( $result->favorite ? ' premium' : '' ) .  ( $repexample || $output_loan_tags ? ' tag-rep-more' : '') ] ) );

				

				if ( ( null !== $slider && ! $slider->isPdf() ) || false === (bool) $result->ej_partner ) {

				/**/
				if( $repexample || $output_loan_tags ){
				$table->addRow( new Row( [
				new Data( 
						( ( null !== $slider && ! $slider->isPdf() ) || false === (bool) $result->ej_partner ? '<i title="' . __( 'More information', 'fs' ) . '" class="toggle-details fa fa-plus" >' . __( 'More information', 'fs' ) . '</i>' : '' ) .
					          ( $output_loan_tags ? '<div class="loan-tags">' . $output_loan_tags . '</div>' : '' ) . 
					          ( $repexample ? '<div class="representative-example">' . $repexample . '</div>' : ''), [ 'class' => 'tag-example-column' ] ),
				], [ 'class' => 'tag-example' . ( $result->favorite ? ' premium' : '' ) ] ) );
				}
				/**/
					$table->addRow( new Row( [

						new Data( '<strong>' . __( 'Borrow up to:', 'fs' ) . '</strong>&nbsp;<br />' . Util::moneyFormat( $result->amount_range_maximum ) . '&nbsp;' . __( 'usd', 'fs' ) . '<br /><br /><strong>' .
						          __( 'Estimated pay back:', 'fs' ) . '</strong>&nbsp;<br />' . $date->format( 'd-m-Y' ), [ 'colspan' => 2 ] ),
						new Data( '<strong>' . __( 'Bad credit history:', 'fs' ) . '</strong>&nbsp;<br/><p class="' . ( $result->bad_history ? 'true' : 'false' ) . '" ></p><br/><br/><strong>' .
						          __( 'Weekend payout:', 'fs' ) . '</strong>&nbsp;<br /><p class="' . ( $result->weekend_payout ? 'true' : 'false' ) . '"></p>', [ 'colspan' => 2 ] ),
						new Data( '<strong>' . __( 'Age:', 'fs' ) . '</strong>&nbsp;<br />' . $pod->display( 'minalder' ) . '<br /><br /><strong>' .
						          __( 'Credit check:', 'fs' ) . '</strong>&nbsp;<br />' . $pod->display( 'credit_check' ), [ 'colspan' => 2 ] ),
						new Data( '<strong>' . __( 'Minimum Income:', 'fs' ) . '</strong>&nbsp;<br />' . ($pod->display( 'minimum_inkomst' )? $pod->display( 'minimum_inkomst' ) : 'n/a') . '<br /><br /><strong>' .
						          __( 'Banks:', 'fs' ) . '</strong>&nbsp;<br />' . ($pod->display( 'banks') ? $pod->display( 'banks', [ 'serial_params' => [ 'and' => __( 'and', 'fs' ) ] ] ) : 'n/a'), [ 'colspan' => 2 ] ),
						new Data( '<i class="mega-icon-eraser report"><a href="#" class="report-data" title="' . __( 'Wrong data? Report this item', 'fs' ) . '">' . __( 'Report incorrect data', 'fs' ) . '</a></i><a href="' . get_permalink( $result->ID ) . '" class="blue-border">' . __( 'Details', 'fs' ) . '</a>', [ 'colspan' => 2, 'class' => 'full-width' ] ),
					], [ 'class' => 'details' ] ) );
			$pod->fetch();	
			}
				
			}
			
			echo $table->render();
		} else {

			/*$table->addRow( new Row( [ new Data( __( 'No loan companies found in that search. Try using less filters.', 'fs' ), [ 'colspan' => 100 ] ) ] ) );*/
			echo '<div class="msg warning slider-msg"> '. __( 'No loan companies found in that search. Try using less filters, or consider the recommended lenders below based on best ratings.', 'fs' ).'</div>';
			
			echo do_shortcode( '[top_rated_companies title="Top Rated Companies" limit="5" type="loan_company"]' );
		}
		

	}
}

?>