<?php


namespace Financer\FilterSlider\Table;


use Financer\FilterSlider\Abstracts\Slider;
use Financer\FilterSlider\Abstracts\Table;
use Financer\FilterSlider\Interfaces\TableInterface;
use Financer\FilterSlider\Surface\Data;
use Financer\FilterSlider\Surface\Row;
use Financer\FilterSlider\Surface\Surface;
use Financer\FilterSlider\Util;

/**
 * Class InterestFreeTable
 * @package Financer\FilterSlider\Table
 */
class InterestFreeTable extends Table implements TableInterface {

	/**
	 * @param null|\Pods $pod
	 *
	 * @param Slider     $slider
	 *
	 * @return void
	 * @internal param null $postType
	 *
	 * @internal param array $query
	 */
	public static function build( \Pods $pod, Slider $slider = null ) {
		$query = $pod->data();
		
		if ( ! $query ) {
			$query = [];
		}
		$table = new Surface( [ 'class' => 'table table-striped' ] );

		$table->setHead( new Row( [
			new Data( __( 'Company', 'fs' ), [ 'title' => __( 'Company', 'fs' ) ] ),
			new Data( __( 'Interest Free Amount', 'fs' ), [ 'title' => __( 'Interest Free Amount', 'fs' ) ] ),
			new Data( __( 'Interest Free Period', 'fs' ), [ 'title' => __( 'Interest Free Period', 'fs' ) ] ),
			new Data( __( 'Min. age', 'fs' ), [ 'class' => 'sliderm', 'title' => __( 'The minimum age you need to borrow money', 'fs' ) ] ),
			new Data( __( 'Bad credit history', 'fs' ), [ 'class' => 'sliderm', 'title' => __( 'The policy regarding a bad credit history', 'fs' ) ] ),
			__( 'Read more', 'fs' ),
		] ) );
		if ( count( $query ) > 0 ) {

			foreach ( $query as $pos => $result ) {
				$table->addRow(
					new Row(
						[
							// Logo
							new Data( '<a href="' . get_permalink( $result->ID ) . '">' . '<img title="' . $result->title . '" src="' . $pod->field( 'logo._src' ) . '"/>' . '</a>' . self::showStars( $result->ID ), [
								'class' => 'vit ' . ( $result->favorite ? 'premium' : '' )
							] ),
							// Loan Amount
							new Data( '<span class="mobile-only">' . __( 'Loan amount:', 'fs' ) . '</span>'. Util::moneyFormat( $result->amount_range_minimum ) . ' - ' . Util::moneyFormat( $result->amount_range_maximum ) . ' ' . __( 'usd', 'fs' ) ),
							// Loan Period
							new Data( '<span class="mobile-only">' . __( 'Loan period:', 'fs' ) . '</span> ' . ( $result->period_range_minimum == $result->period_range_maximum ? Util::getPeriod( $result->period_range_minimum ) : Util::getPeriod( $result->period_range_minimum ) . ' - ' . Util::getPeriod( $result->period_range_maximum ) ) ),
							// Minimum Age
							new Data( '<span class="mobile-only">' . __( 'Minimum age:', 'fs' ) . '</span> ' . $result->minalder, [ 'class' => 'minimum-age' ] ),
							// Bad History
							new Data( '<span class="mobile-only">' . __( 'Bad credit history', 'fs' ) . '</span><p class="' . ( $result->bad_history ? 'true' : 'false' ) . '"></p>', [ 'class' => 'sliderm' ] ),
							// See more
							'<a href="' . user_trailingslashit( get_permalink( $result->ID ) . 'redirect' ) . '" class="button small applyYellow" target="_blank" rel="nofollow" title="' . __( 'Borrow money from', 'fs' ) . ' ' . $result->title . '">' . __( 'Apply now', 'fs' ) . '</a>' . ( $result->ej_partner ? '' : '<a href="' . get_permalink( $result->ID ) . '" class="applyNow">' . __( 'Read more', 'fs' ) . '</a>' ),
						], [ 'class' => ( $pos % 2 ? 'even' : 'odd' ) . ( $result->ej_partner ? ' greyed' : '' ) . ( $result->favorite ? ' premium' : '' ) ]
					)
				);
				$pod->fetch();
			}
		} else {
			$table->addRow( new Row( [ __( 'No interest free companies found.', 'fs' ) ] ) );
		}
		echo $table->render();
	}
}