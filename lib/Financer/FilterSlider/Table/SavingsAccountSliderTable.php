<?php

namespace Financer\FilterSlider\Table;


use Financer\FilterSlider\Abstracts\Slider;
use Financer\FilterSlider\Abstracts\Table;
use Financer\FilterSlider\Interfaces\TableInterface;
use Financer\FilterSlider\SortUtil;
use Financer\FilterSlider\Util;
use Financer\FilterSlider\Surface\Data;
use Financer\FilterSlider\Surface\Row;
use Financer\FilterSlider\Surface\Surface;

/**
 * Class SavingsAccountSliderTable
 * @package Financer\FilterSlider\Table
 */
class SavingsAccountSliderTable extends Table implements TableInterface {

	/**
	 * @param null|\Pods $pod
	 *
	 * @param Slider     $slider
	 *
	 * @return void
	 *
	 * @internal param array $query
	 */
	public static function build( \Pods $pod, Slider $slider = null ) {
		$query = $pod->data();
		if ( ! $query ) {
			$query = [];
		}
		$table = new Surface( [ 'class' => 'table table-striped' ] );
		$table->setHead(
			[
				new Data(
					__( 'Bank', 'fs' ), [
						'title' => __( 'Name of the bank', 'fs' ),
						'class' => 'vit',
					]
				),
				new Data( __( 'Saving Account', 'fs' ), [ 'title' => __( 'Name of the savings account', 'fs' ) ] ),
				new Data(
					__( 'Amount', 'fs' ), [
						'title' => __( 'The amount of money you are saving', 'fs' ),
						'class' => 'sliderm',
					]
				),
				new Data(
					__( 'Period', 'fs' ), [
						'title' => __( 'The period of time you are saving', 'fs' ),
						'class' => 'sliderm',
					]
				),
				new Data( __( 'Interest rate', 'fs' ), [ 'title' => __( 'The current interest rate for the savings account', 'fs' ) ] ),
				new Data( __( 'Profit', 'fs' ), [ 'title' => __( 'The total amount of profit during the whole saving period', 'fs' ) ] ),
				new Data(
					__( 'Free Withdrawals', 'fs' ), [
						'title' => __( 'The amount of free withdrawals you are allowed to during the saving period', 'fs' ),
						'class' => 'sliderm',
					]
				),
				new Data(
					__( 'Gov Guarantee', 'fs' ), [
						'title' => __( 'If the bank is backed up with a governmental guarantee or not', 'fs' ),
						'class' => 'sliderm',
					]
				),
				new Data( __( 'Review', 'fs' ), [ 'title' => __( 'Read more about the bank', 'fs' ) ] ),
			]
		);
		if ( count( $query ) > 0 ) {
			$period = $slider->getPeriod();
			$period = $period < 1 ? Util::getPeriod( $period * 365 ) : $period . ' ' . __( 'Years', 'fs' );
			foreach ( $query as &$result ) {
				$result->logo = $pod->field( 'bank.logo._src' );
				$pod->fetch();
			}
			unset( $result );
			foreach ( $query as $pos => $result ) {
				if ( $result->floating_rate ) {
					$small = '<span class="yellowSpan">' . __( 'Floating interest', 'fs' ) . '</span>';
				} else {
					$small = '<span class="greenSpan">' . __( 'Fixed interest', 'fs' ) . '</span>';

				}
				$profit = round( (int) $slider->getAmount() * pow( 1 + (float) $result->interest_rate / 100, (int) $slider->getPeriod() ) - (int) $slider->getAmount(), 2 );
				$table->addRow( new Row(
						[
							// Logo
							new Data( '<i class="mega-icon-eraser report"><a title="' . __( 'Wrong data? Report this item', 'fs' ) . '"></a></i><img title="' . $result->bank_title . '" src="' . $result->logo . '"/>' . self::showStars( $result->bank_id ) . ' <span class="totalReviews"><a href="' . get_permalink( $result->bank_id ) . '#reviews">' . __( 'Read', 'fs' ) . '&nbsp;' . $result->total_reviews . ' ' . __( 'reviews.', 'fs' ) . '</a></span>', [ 'class' => 'sliderm vit' . ( $result->favorite ? '  premium' : '' ) . ( $result->ej_partner ? ' np' : '' ) ] ),
							// Title
							new Data( '<span class="mobile-only">' . __( 'Saving account:', 'fs' ) . '</span> ' . $result->title, [ 'class' => 'smaller-text' ] ),
							// Minimum Save Amount
							new Data( '<span class="mobile-only">' . __( 'Saving amount:', 'fs' ) . '</span> ' . Util::moneyFormat( $slider->getAmount() ) . ' ' . __( 'usd', 'fs' ), [ 'class' => 'sliderm' ] ),
							// Period
							new Data( '<span class="mobile-only">' . __( 'Saving time:', 'fs' ) . '</span> ' . $period, [ 'class' => 'sliderm' ] ),
							// Interest Rate
							new Data( '<span class="mobile-only">' . __( 'Saving interest rate:', 'fs' ) . '</span> ' . ( ( ! empty( $result->interest_rate ) || $result->interest_rate == 0 ) ? $result->interest_rate . '%' : '&nbsp;' ) . '<br />' . $small ),
							// Profit
							new Data( '<span class="mobile-only">' . __( 'Saving profit:', 'fs' ) . '</span> ' . $profit . ' ' . __( 'usd', 'fs' ) ),
							// Free Widthdrawls
							new Data( '<span class="mobile-only">' . __( 'Free withdrawals:', 'fs' ) . '</span> ' . ( $result->free_withdrawals == - 1 ? __( 'N/A', 'fs' ) : $result->free_withdrawals ), [ 'class' => 'sliderm' ] ),
							// Governmental Guarantee
							new Data( '<span class="mobile-only">' . __( 'Governmental guarantee:', 'fs' ) . '</span><p class="' . ( $result->governmental_guarantee ? 'true' : 'false' ) . '"></p>', [ 'class' => 'sliderm' ] ),
							// Apply/Review
							'<a href="' . user_trailingslashit( get_permalink( $result->bank_id ) . 'redirect' ) . '" class="button ' . ( $result->ej_partner ? 'small grey' : 'small applyYellow' ) . '" target="_blank" rel="nofollow" title="' . __( 'Apply at', 'fs' ) . ' ' . $result->bank_title . '">' . __( 'Open account', 'fs' ) . '</a>' . ( $result->ej_partner ? '' : '<a href="' . get_permalink( $result->bank_id ) . '" class="applyNow">' . __( 'Read more', 'fs' ) . '</a>' ),
						], [ 'data-id' => $result->ID, 'class' => ( ( $pos % 2 ) ? 'even' : 'odd' ) . ( $result->favorite ? '  premium' : '' ) ] )
				);
			}
		} else {
			$table->addRow( new Row( [ new Data( __( 'No savings accounts found in that search. Try using less filters.', 'fs' ), [ 'colspan' => 100 ] ) ] ) );
		}
		echo $table->render();
	}
}
