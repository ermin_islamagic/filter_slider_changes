<?php

namespace Financer\FilterSlider\Table;


use Financer\FilterSlider\Abstracts\Slider;
use Financer\FilterSlider\Abstracts\Table;
use Financer\FilterSlider\Interfaces\TableInterface;
use Financer\FilterSlider\SortUtil;
use Financer\FilterSlider\Util;
use Financer\FilterSlider\Surface\Data;
use Financer\FilterSlider\Surface\Row;
use Financer\FilterSlider\Surface\Surface;

/**
 * Class CreditcardTable
 * @package Financer\FilterSlider\Table
 */
class CreditcardTable extends Table implements TableInterface {

	/**
	 * @param null|\Pods  $pod
	 *
	 * @param Slider|null $slider
	 *
	 * @return void
	 *
	 * @internal param array $query
	 */
	public static function build( \Pods $pod, Slider $slider = null ) {
		$query = $pod->data();
		if ( ! $query ) {
			$query = [];
		}
		$table = new Surface( [ 'class' => 'table table-striped' ] );
		$table->setHead(
			new Row(
				[
					new Data( __( 'Credit card', 'fs' ), [ 'title' => __( 'Logo for creditcard', 'fs' ), 'class' => 'vit', ] ),
					new Data(
						__( 'Max credit', 'fs' ), [
							'title' => __( 'Max standard credit', 'fs' ),
							'class' => 'sliderm',
						]
					),
					new Data(
						__( 'Interest free days', 'fs' ), [
							'title' => __( 'How many days without interest', 'fs' ),
							'class' => 'sliderm',
						]
					),
					new Data(
						__( 'Interest', 'fs' ), [
							'title' => __( 'Total interest', 'fs' ),
						]
					),
					new Data(
						__( 'Fees', 'fs' ), [
							'title' => __( 'Fees and other costs', 'fs' ),
							'class' => 'sliderm',
						]
					),
					new Data(
						__( 'Travel insurance', 'fs' ), [
							'title' => __( 'Is travel insurance included in this card?', 'fs' ),
							'class' => 'sliderm',
						]
					),
					new Data(
						__( 'Withdrawal fee', 'fs' ), [
							'title' => __( 'Cost of using the card in an ATM', 'fs' ),
							'class' => 'sliderm',
						]
					),
					new Data(
						__( 'Card type', 'fs' ), [
							'title' => __( 'Card type', 'fs' ),
							'class' => 'sliderm',
						]
					),
					new Data( __( 'Apply', 'fs' ), [ 'title' => __( 'Apply for the card below', 'fs' ) ] ),
				]
			)
		);
		if ( count( $query ) > 0 ) {
			foreach ( $query as $pos => $result ) {
				$func = function ( array $attr ) use ( $result ) {
					$attr['alt'] = get_the_title( $result->ID );

					return $attr;
				};
				add_filter( 'wp_get_attachment_image_attributes', $func );
				$pod->fetch( $result->ID );
				$days = (float) $result->period;
				$table->addRow(
					new Row(
						[
							// Logo
							new Data( '<i class="mega-icon-eraser report"><a href="#" title="' . __( 'Wrong data? Report this item', 'fs' ) . '">&nbsp;</a></i>' . ( null !== $slider && ! $slider->isPdf() && ! empty( $result->card_details ) ? '<i title="' . __( 'More information', 'fs' ) . '" class="toggle-details fa fa-plus" >' . __( 'More information', 'fs' ) . '</i>' : '' ) . '' . $pod->display( 'logo._img.full' ) . '', [
								'class' => 'vit cctd' . ( $result->featured ? ' premium' : '' )
							] ),
							// Max Credit
							new Data( '<span class="mobile-only">' . __( 'Max credit:', 'fs' ) . '</span> ' . ( _isset( $result->summ ) ? ( - 1 == $result->summ ? __( 'N/A', 'fs' ) : Util::moneyFormat( $result->summ ) . ' ' . __( 'usd', 'fs' ) ) : '&nbsp;' ), [ 'class' => 'max-credit', ] ),
							// Interest Free Days
							new Data( '<span class="mobile-only">' . __( 'Interest free days:', 'fs' ) . '</span> ' .  $days . ' ' . ( - 1 == $days ? __( 'N/A', 'fs' ) : ( ( 1 < $days || 0 == $days )
									? __( 'days', 'fs' ) : __( 'day', 'fs' ) ) ), [ 'class' => 'sliderm' ] ),
							// Loan Percent
							new Data( '<span class="mobile-only">' . __( 'Interest rate:', 'fs' ) . '</span><strong class="cardInterest">' . ( _isset( $result->percent ) ? ( - 1 == $result->percent ? __( 'N/A', 'fs' ) : $result->percent . '%' ) : '&nbsp;' ) . '</strong>' ),
							// Loan Fees
							new Data( '<span class="mobile-only">' . __( 'Card fee:', 'fs' ) . '</span> ' . ( _isset( $result->looses ) ? Util::moneyFormat( $result->looses ) . ' ' . __( 'usd', 'fs' ) : '&nbsp;' ), [ 'class' => 'card-fee' ] ),
							// Travel Insurance
							new Data( '<span class="mobile-only">' . __( 'Travel insurance:', 'fs' ) . '</span> ' . ( $result->travel_insurance ? ( '<p class="true" />' ) : ( '<p class="false" />' ) ), [ 'class' => 'travel-insurance' ] ),
							// Withdrawl Fee
							new Data( '<span class="mobile-only">' . __( 'ATM fee:', 'fs' ) . '</span> ' . ( _isset( $result->atm_fee ) ? ( ( - 1 == $result->atm_fee ) ? __( 'N/A', 'fs' ) : Util::numberFormat( $result->atm_fee ) . '% ' ) : '&nbsp;' ), [ 'class' => 'atm-fee' ] ),
							// Card Type
							new Data( '<span class="mobile-only">' . __( 'Card type:', 'fs' ) . '</span> ' . ( _isset( $result->card_type ) ? '<p class="' . $result->card_type . '"></p>' : '&nbsp;' ), [ 'class' => 'card-type' ] ),
							// Loan Apply
							'<a href="' . user_trailingslashit( $pod->field( 'permalink' ) . 'redirect' ) . '" class="button small ' . ( $result->affiliate ? 'applyYellow' : 'grey' ) . '" target="_blank" rel="nofollow" title="' . sprintf( __( 'Apply for %s', 'fs' ), $result->title ) . '"> ' . __( 'Application', 'fs' ) . ' </a>',
						], [ 'data-period' => $days, 'class' => ( ( $pos % 2 ) ? 'even' : 'odd' ) . ( $result->featured ? '  premium' : '' ) ] )
				);
				if ( null !== $slider && ! $slider->isPdf() && ! empty( $result->card_details ) ) {
					$table->addRow( new Row( [
						new Data( $result->card_details ),
					], [ 'class' => 'details' ] ) );
				}
				remove_filter( 'wp_get_attachment_image_attributes', $func );
			}
		} else {
			$table->addRow( new Row( [ new Data( __( 'No credit cards found in your search. Try using less filters.', 'fs' ), [ 'colspan' => 100 ] ) ] ) );
		}
		echo $table->render();
	}

}
