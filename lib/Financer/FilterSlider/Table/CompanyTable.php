<?php

namespace Financer\FilterSlider\Table;


use Financer\FilterSlider\Abstracts\Slider;
use Financer\FilterSlider\Abstracts\Table;
use Financer\FilterSlider\Interfaces\TableInterface;
use Financer\FilterSlider\Util;
use Financer\FilterSlider\Surface\Data;
use Financer\FilterSlider\Surface\Row;
use Financer\FilterSlider\Surface\Surface;

/**
 * Class LoanTable
 * @package Financer\FilterSlider\Table
 */
class CompanyTable extends Table implements TableInterface {
	/**
	 * @param null|\Pods  $pod
	 *
	 * @param Slider|null $slider
	 *
	 * @return void
	 *
	 */
	public static function build( \Pods $pod, Slider $slider = null ) {
		$query = $pod->data();
		if ( ! $query ) {
			$query = [];
		}

		$generalSettings = pods( 'general_settings' );

		$sliderSetting = pods( 'slider_settings' );
		$remove_apr = $sliderSetting->field( 'remove_apr' );
		$remove_style = "";
		if($remove_apr==1){
			$remove_style = "display:none;";
		}


		$table = new Surface( [ 'class' => 'table table-striped' ] );
		$table->setHead( new Row( [
			new Data( __( 'Loan company', 'fs' ), [ 'title' => __( 'Logo of the loan company', 'fs' ), 'class' => 'vit' ] ),
			new Data( __( 'Loan amount', 'fs' ), [ 'title' => __( 'The range of loan amounts possible to borrow', 'fs' ) ] ),
			new Data( __( 'Loan period', 'fs' ), [
				'title' => __( 'The loan period you are searching for. The lender may lend in shorter or longer periods', 'fs' ), 'class' => 'sliderm',
			] ),
			new Data( __( 'APR', 'fs' ), [ 'title' => __( 'The annual interest rate (not effective rate)', 'fs' ), 'class' => 'sliderm', 'style'=>$remove_style ] ),
			new Data( __( 'Bad credit history', 'fs' ), [ 'class' => 'sliderm', 'title' => __( 'The policy regarding a bad credit history', 'fs' ) ] ),
			new Data( __( 'Weekend payout', 'fs' ), [ 'class' => 'sliderm', 'title' => __( 'Possibility to get payout during weekends', 'fs' ) ] ),
			new Data( __( 'Min. age', 'fs' ), [ 'class' => 'sliderm', 'title' => __( 'The minimum age you need to borrow money', 'fs' ) ] ),
			__( 'Read more', 'fs' ),
		] ) );
		if ( count( $query ) > 0 ) {

			foreach ( $query as $pos => $result ) {
				$filter = pods(
					'loan_dataset', [
						'select' => [
							'min( amount_range_minimum ) AS min',
							'max( amount_range_maximum ) AS max',
						],
						'where'  => [
							'company_parent.ID' => $result->ID,
						],
					]
				);

				$filter2 = pods(
					'loan_dataset', [
						'select' => [
							'min( period_range_minimum ) AS min',
							'max( period_range_maximum ) AS max',
						],
						'where'  => [
							'company_parent.ID' => $result->ID,
						],
					]
				);

				$filter3 = pods(
					'loan_dataset', [
						'select' => [
							'min( interest_rate ) AS min',
							'max( highest_annual_interest_rate ) AS max',
						],
						'where'  => [
							'company_parent.ID' => $result->ID,
						],
					]
				);
				$table->addRow(
					new Row(
						[
							// Logo
							new Data( '<a href="' . get_permalink( $result->ID ) . '">' . '<img title="' . $result->title . '" src="' . $pod->field( 'logo._src' ) . '"/>' . '</a>' . self::showStars( $result->ID ) . ' <span class="totalReviews"><a href="' . get_permalink( $result->ID ) . '#read-reviews">' . __( 'Read', 'fs' ) . '&nbsp;' . $result->total_reviews . ' ' . __( 'reviews.', 'fs' ) . '</a></span>' . '<span class="sort-company" style="display: none;">' . preg_replace('/\PL/u', '', lcfirst($result->title)) . '</span><span class="sort-rating" style="display: none;">' . $result->rating . '</span>', [
								'class' => 'vit ' . ( $result->favorite ? 'premium' : '' )
							] ),
							// Loan Amount
							new Data( '<span class="mobile-only">' . __( 'Loan amount:', 'fs' ) . '</span> ' . Util::moneyFormat( $filter->field( 'min' ) ) . ' - ' . Util::moneyFormat( $filter->field( 'max' ) ) . ' ' . __( 'usd', 'fs' ), [ 'class' => 'loan-amount' ] ),
							// Loan Period
					new Data( '<span class="mobile-only">' . __( 'Loan period:', 'fs' ) . '</span> ' . Util::getPeriod( $filter2->field( 'min' ) ) . ' - ' . Util::getPeriod( $filter2->field( 'max' ) ), [ 'class' => 'loan-period' ] ),
					// Loan APR numberFormat
					new Data( '<span class="mobile-only">' . __( 'Nominal APR:', 'fs' ) . '</span> ' . Util::numberFormat( $filter3->field( 'min' ) ) . '%' . ( $filter3->field( 'max' ) != '0' ? ' - ' . Util::numberFormat( $filter3->field( 'max' ) ) . '%' : '' ), [ 'class' => 'loan-apr', 'style'=>$remove_style ] ),
							// Bad History
							new Data( '<span class="mobile-only">' . __( 'Accepts bad credit:', 'fs' ) . '</span><p class="' . ( $result->bad_history ? 'true' : 'false' ) . '"></p></span>', [ 'class' => 'bad-credit' ] ),  
							// Weekend Payout
							new Data( '<span class="mobile-only">' . __( 'Weekend payout:', 'fs' ) . '</span><p class="' . ( $result->helgutbetalning ? 'true' : 'false' ) . '"></p>', [ 'class' => 'weekend-payout' ] ),
							// Minimum Age
							new Data( '<span class="mobile-only">' . __( 'Minimum age:', 'fs' ) . '</span> ' . $result->minalder, [ 'class' => 'minimum-age' ] ),
							// See more
							new Data( '<a href="' . user_trailingslashit( get_permalink( $result->ID ) . 'redirect' ) . '?b=t" class="button small applyYellow" target="_blank" rel="nofollow" title="' . __( 'Borrow money from', 'fs' ) . ' ' . $result->title . '">' . __( 'Apply now', 'fs' ) . '</a>' . ($generalSettings->field('show_sponsored_text')? '<div class="sponsored">' . __( 'Sponsored', 'fs' ) . '</div>': '') . '<a href="' . get_permalink( $result->ID ) . '" class="applyNow">' . __( 'Read more', 'fs' ) . '</a>', [ 'class' => 'loan-apply' ] ),
						], [ 'class' => ( $pos % 2 ? 'even' : 'odd' ) . ( $result->ej_partner ? ' greyed' : '' ) . ( $result->favorite ? ' premium' : '' ) . ( $result->custom_representative_example ? ' tag-rep-more' : '') ]
					)
				);

				/**/
				if( $result->custom_representative_example ){
					$table->addRow( 
						new Row( [
							new Data( '<div class="representative-example">' . $result->custom_representative_example . '</div>', [ 'class' => 'tag-example-column' ])
						], [ 'class' => 'tag-example' ])
					);
				}

				$pod->fetch();
			}
		} else {
			$table->addRow( new Row( [ new Data( __( 'No loan companies found in that search. Try using less filters.', 'fs' ), [ 'colspan' => 100 ] ) ] ) );
		}
		echo $table->render();
		?>

		<?php
	}
}
