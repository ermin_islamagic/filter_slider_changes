<?php

namespace Financer\FilterSlider\Table;


use Financer\FilterSlider\Abstracts\Slider;
use Financer\FilterSlider\Abstracts\Table;
use Financer\FilterSlider\Interfaces\TableInterface;
use Financer\FilterSlider\SortUtil;
use Financer\FilterSlider\Util;
use Financer\FilterSlider\Surface\Data;
use Financer\FilterSlider\Surface\Row;
use Financer\FilterSlider\Surface\Surface;

/**
 * Class MortgageSliderTable
 * @package Financer\FilterSlider\Table
 */
class MortgageSliderTable extends Table implements TableInterface {
	/**
	 * @param null|\Pods $pod
	 *
	 * @param Slider     $slider
	 *
	 * @return void
	 * @internal param null $postType
	 *
	 * @internal param array $query
	 */
	public static function build( \Pods $pod, Slider $slider = null ) {
		$query = $pod->data();
		if ( ! $query ) {
			$query = [];
		}
		$table = new Surface( [ 'class' => 'table table-striped mortgage_table' ] );
		if ( $slider->getPeriod() < 1 ) {
			$field = ( $slider->getPeriod() * 12 ) . '_mir';
		} else {
			$field = $slider->getPeriod() . '_yir';
		}
		$table->setHead(
			new Row(
				[
					new Data( __( 'Bank', 'fs' ), [ 'title' => __( 'Name of the bank', 'fs' ) ] ),
					new Data( __( 'Mortgage interest', 'fs' ) . ' ' . Util::getPeriod( $slider->getPeriod() * 365 ), [ 'title' => __( 'The period of the fixed interest rate', 'fs' ) ] ),
					new Data( __( 'Monthly interest', 'fs' ), [ 'title' => __( 'An estimation of the monthly fee from the interest rate, not including installments', 'fs' ) ] ),
					new Data( __( 'Other fees', 'fs' ), [ 'title' => __( 'The total fees, like start and maintenance fees of the mortgage', 'fs' ) ] ),
					new Data( __( 'More info', 'fs' ), [ 'title' => __( 'Find more information about the bank', 'fs' ) ] ),
				]
			)
		);
		if ( count( $query ) > 0 ) {
			foreach ( $query as $pos => $result ) {
				$pod->fetch( $result->ID );
				$interest = ! empty( $result->$field ) ? $result->$field : 0;
				$table->addRow( new Row(
						[
							// Logo
							new Data( '<i class="mega-icon-eraser report"><a href="#" title="' . __( 'Wrong data? Report this item', 'fs' ) . '"></a></i><img title="' . $result->bank_title . '" src="' . $pod->field( 'bank.logo._src' ) . '"/>' . self::showStars( $result->bank_id )
								 . ' <span class="totalReviews"><a href="' . get_permalink( $result->bank_id ) . '#reviews">' . __( 'Read', 'fs' ) . '&nbsp;' . $result->total_reviews . ' ' . __( 'reviews.', 'fs' ) . '</a></span>', [ 'class' => 'vit' . ( $result->ej_partner ? ' np' : '' ) ] )
							,
							// Interest
							new Data( '<span class="mobile-only">' . __( 'Mortgage rate:', 'fs' ) . '</span> ' . ( _isset( $interest ) ? ( - 1 == $interest ? __( 'N/A', 'fs' ) : Util::numberFormat( $interest ) . '%' ) : '&nbsp;' ) ),
							// Montly Cost
							- 1 == $interest ? __( 'N/A', 'fs' ) :
							new Data( '<span class="mobile-only">' . __( 'Estimated monthly cost', 'fs' ) . '</span> ' . Util::moneyFormat( round( ( $slider->getAmount() * ( (float) $interest / 100 ) ) / 12, 2 ) ) . ' ' . __( 'usd', 'fs' ) ),
							// Total Fees
							new Data( '<span class="mobile-only">' . __( 'Mortgage fee', 'fs' ) . '</span> ' . ( $result->total_fees == - 1 ? __( 'N/A', 'fs' ) : Util::moneyFormat( $result->total_fees ) . ' ' . __( 'usd', 'fs' ) ) ),
							// Apply/Review
							'<a href="' . user_trailingslashit( get_permalink( $result->bank_id ) . 'redirect' ) . '" rel="nofollow" class="button small applyYellow" title="' . __( 'Apply at', 'fs' ) . ' ' . $result->bank_title . '"> ' . __( 'Application', 'fs' ) . ' </a>' . ( $result->ej_partner ? '' : '<a href="' . ( ! empty( $result->url ) ? $result->url : get_permalink( $result->bank_id ) ) . '" class="applyNow" target="_blank">' . __( 'Read more', 'fs' ) . '</a>' ),
						], [ 'data-id' => $result->ID, 'class' => ( ( $pos % 2 ) ? 'even' : 'odd' ) . ( $result->favorite ? ' premium' : '' ) . ( $result->ej_partner ? ' greyed' : '' ) ] )
				);
			}
			/** @noinspection PhpUndefinedMethodInspection */

		} else {
			$table->addRow( new Row( [ new Data( __( 'No mortgages found in that search.', 'fs' ), [ 'colspan' => 100 ] ) ] ) );
		}
		echo $table->render();
		if ( count( $query ) > 0 ) :
			?>

			<?php
		endif;
	}
}
