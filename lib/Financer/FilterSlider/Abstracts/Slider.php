<?php
/**
 * Base Slider
 *
 * PHP version 5.4
 *
 * @category Slider
 *
 * @package Slider
 *
 * @author Derrick Hammer <derrick@derrickhammer.com>
 *
 * @license Propietary https://en.wikipedia.org/wiki/Proprietary_software
 *
 * @link http://www.financer.com
 */

namespace Financer\FilterSlider\Abstracts;

use Dompdf\Dompdf;
use Financer\FilterSlider\Plugin;
use Financer\FilterSlider\Shortcode\Social;

use Financer\FilterSlider\Util;

/**
 * Class Slider
 *
 * @category Slider
 *
 * @package Slider
 *
 * @author Derrick Hammer <derrick@derrickhammer.com>
 *
 * @license Propietary https://en.wikipedia.org/wiki/Proprietary_software
 *
 * @link http://www.financer.com
 */
abstract class Slider {

	/**
	 * Period to cache a db query
	 * @var int
	 */
	const CACHE_PERIOD = HOUR_IN_SECONDS;
	const LOAN_TOTAL_SQL = [
		'(1+((loan_datasets.d.interest_rate/100) * (1/12))) monthly_rate',
		'({AMOUNT} * POW((SELECT monthly_rate), ({PERIOD} / 30))*((1-(SELECT monthly_rate))/(1-POW((SELECT monthly_rate),({PERIOD} / 30))))) annuity',
		'(IF(loan_datasets.d.fee_flat = 0 OR loan_datasets.d.fee_flat IS NULL, IF (loan_datasets.d.fee_percent = 0 OR loan_datasets.d.fee_percent IS NULL, 0, (loan_datasets.d.fee_percent / 100) * {AMOUNT}), loan_datasets.d.fee_flat)) fee',
		'((SELECT fee) + (IF((SELECT annuity) IS NULL, 0,(SELECT annuity)) * ({PERIOD}/30)) +  (IF (loan_datasets.d.monthly_fee IS NULL, 0, monthly_fee) *  IF({PERIOD} < 30, 1, {PERIOD}/30)) - IF((SELECT annuity) IS NULL or (SELECT annuity) = 0, 0,{AMOUNT})) total_cost',
		'(((SELECT total_cost) + {AMOUNT})/ IF({PERIOD} < 30, 1, {PERIOD}/30) ) total_monthly_payback'
	];
	/**
	 * @var int
	 */
	protected static $instance = 0;
	/**
	 * Period to cache a db query
	 * @var array
	 */
	protected $query = [];
	/**
	 * Slider amount
	 * @var float
	 */
	protected $amount = 0;
	/**
	 * Slider period
	 * @var int
	 */
	protected $period = 0;
	/**
	 * Slider period
	 * @var int
	 */
	protected $input_age = 0;
	/**
	 * Class to use for rendering result table
	 * @var string
	 */
	protected $tableClass = 'SliderLoanTable';
	/**
	 * WordPress post type to query
	 * @var string
	 */
	protected $postType = 'company_single';
	/**
	 * Allowed custom parameters
	 * @var array
	 */
	protected $paramsList = [ 'amount', 'period', 'maxAmount', 'maxPeriod', 'filters', 'excludeFilters', 'newsletterGroup', 'limit', 'show_limit', 'show_all', 'title', 'input_age', 'tags', 'age_dropdown' ];

	/**
	 * Allowed custom parameters that can be seen in the query url
	 * @var array
	 */
	protected $shareableParamsList = [ 'amount', 'period', 'limit', 'tags' ];
	/**
	 * @var array
	 */
	protected $paramsNoSanitize = [ 'title' ];
	/**
	 * Fields to query
	 * @var array
	 */
	protected $fields = [];
	/**
	 * Extra joins to add
	 * @var array
	 */
	protected $joins = [];
	/**
	 * If the post type has a company relation
	 * @var bool
	 */
	protected $hasCompany = true;
	/**
	 * Limit results
	 * @var int
	 */
	protected $limit = 50;
	/**
	 * Maximum amount to show in slider options
	 * @var int
	 */
	protected $maxAmount = 0;
	/**
	 * Minimum amount to show in slider options
	 * @var int
	 */
	protected $maxPeriod = 0;
	/**
	 * HTML labels for translation
	 * @var array
	 */
	protected $htmlLabels = [];
	/**
	 * List of filters to show on the UI
	 * @var array|string
	 */
	protected $filters = '';
	/**
	 * List of filters to be checked by default
	 * @var array
	 */
	protected $filtersEnabled = [];
	/**
	 * List of filters to exclude on the UI
	 * @var array|string
	 */
	protected $excludeFilters = '';
	/**
	 * Pod instance
	 * @var \Pods
	 */
	protected $pod = null;
	/**
	 * Array of all possible filters
	 * @var array|string
	 */
	protected $filterList = [
		'interest_free',
		'filter_gov',
		'bad_history',
		'filter_age_20',
		'weekend_payout',
		'loan_broker_filter_active',
		'non_affiliate'
	];
	/**
	 * @var
	 */
	protected $newsletterGroup;
	/**
	 * @var array
	 */
	protected $limitList = [
		10  => false,
		20  => false,
		50  => true,
		100 => false,
		- 1 => false
	];
	/**
	 * @var bool
	 */
	protected $limitsEnabled = false;
	/**
	 * @var bool
	 */
	protected $sortEnabled = true;
	/**
	 * @var bool
	 */
	protected $tagsEnabled = null;
	/**
	 * @var string
	 */
	protected $sort = 'total-cost';
	/**
	 * @var string
	 */
	protected $enewsletter_groupname = "lenderupdate";
	/**
	 * @var int
	 */
	protected $totalResults = 0;
	/**
	 * @var int
	 */
	protected $total = 0;
	/**
	 * @var bool
	 */
	protected $isPdf = 0;
	/**
	 * @var integer
	 */
	protected $post_id = null;
	/**
	 * @var string
	 */
	protected $id = null;
	/**
	 * @var string
	 */
	protected $instanceId = null;
	/**
	 * Pod setting storing slider comparison run count
	 * @var string
	 */
	protected $compareCounterSetting = null;
	/**
	 * Pod instance for slider_settings
	 * @var \Pods
	 */
	protected $sliderSettings = null;
	/**
	 * Is debug enabled
	 * @var \Pods
	 */
	protected $debug = false;
	/**
	 * @var
	 */
	protected $show_limit;
	/**
	 * @var
	 */
	protected $title;
	/**
	 * @var bool
	 */
	protected $isAjax = false;
	/**
	 * @var
	 */
	protected $interest_free;
	/**
	 * @var
	 */
	protected $filter_gov;
	/**
	 * @var
	 */
	protected $bad_history;
	/**
	 * @var
	 */
	protected $filter_age_20;
	/**
	 * @var
	 */
	protected $loan_broker_filter_active;
		/**
	 * @var
	 */
	protected $non_affiliate;
	/**
	 * @var
	 */
	protected $weekend_payout;
	protected $loan_company;
	protected $steps = [
		'amount',
		'period',
		'filters',
		'limits_sort',
		'loan_tags',
		'age_dropdown',
		'submit',
	];

	protected $show_all;

	protected $s_atts;

	protected $tags;
	protected $age_dropdown;
	/**
	 * Constructor
	 *
	 * @param array $atts Shortcode attributes
	 */
	public function __construct( $atts ) {

		foreach ( $atts as $key => $val ) {
			if ( strpos( $key, 'param_' ) === 0 ) {
				$param = str_replace( 'param_', '', $key );
				if ( in_array(
					strtolower( $param ), array_map(
						'strtolower',
						$this->paramsList
					)
				) ) {
					if ( is_array( $val ) ) {
						array_walk( $val, 'trim' );
						array_walk( $val, 'sanitize_key' );
					} else if ( preg_match( '/[\w]+,[\w]+,?/', $val ) ) {
						$val = explode( ',', $val );
						array_walk( $val, 'trim' );
						array_walk( $val, 'sanitize_key' );
					} else {
						if ( in_array( $param, $this->paramsNoSanitize ) ) {
							$val = wp_strip_all_tags( $val );

						} else {
							$val = sanitize_key( $val );
						}
					}
					$this->__set( $param, $val );
					
				}
			}
		}

		
		$this->instanceId = strtolower( ( new \ReflectionClass( $this ) )->getShortName() ) . '_' . self::$instance;
		$this->id = strtolower( ( new \ReflectionClass( $this ) )->getShortName() );
		if ( isset( $atts['post_id'] ) ) {
			$this->post_id = (int) $atts['post_id'];
		}
		$this->sliderSettings = pods( 'slider_settings' );
		$this->labels();
		
		
		if($this->sliderSettings->field( 'filter_non_affiliate' )=='1'){
			
		$this->filtersEnabled[] = "non_affiliate";
		$this->non_affiliate = 1;
		}
		
		$this->processFilters( $atts );


		if ( isset( $_GET['slider_debug'] ) && 1 === (int) $_GET['slider_debug'] && pods_is_admin( 'pods' ) ) {
			$this->debug = true;
		}

		if ( ! $this->limitList[ $this->limit ] ) {
			foreach ( $this->limitList as &$limit ) {
				$limit = false;
			}
			$this->limitList[ $this->limit ] = true;
		}

		if(!empty($atts['param_age_dropdown'])){
		$this->age_dropdown = $atts['param_age_dropdown'];
		}


		if(false == empty($atts['param_sort'])){
			$this->sort = $atts['param_sort'];
		}
		$this->s_atts = $atts;
		$this->isAjax         = defined( 'DOING_AJAX' ) && DOING_AJAX;
		

		if($this->sliderSettings->field( 'default_sorting' )){
			$this->sort = $this->sliderSettings->field( 'default_sorting' );
		}
		$this->sortEnabled = $this->sliderSettings->field( 'alphabetical_sorting' );
		if($this->sortEnabled){
			$this->sort = "company-name";
		}
		$this->tagsEnabled = $this->sliderSettings->field( 'show_tags_of_loan_dataset_as_dropdown' );

		if(!empty($atts['param_tags'])){
		$this->tags = $atts['param_tags'];
		}


		$this->buildQuery();


	}

	/**
	 *
	 */
	protected function labels() {
		$this->htmlLabels = [

			'amount'                 => __( 'Loan amount', 'fs' ),
			'period'                 => __( 'Loan period', 'fs' ),
			'age'                 	 => __( 'Input your age', 'fs' ),
			'filters'                => __( 'Optional filters:', 'fs' ),
			'interest_free'          => __( 'interest free loans', 'fs' ),
			'bad_history'            => __( 'lenders accepting bad credit history', 'fs' ),
			'loan_broker_filter_active' => __( 'actual lenders and exclude loan brokers', 'fs' ),
			'non_affiliate' => __( 'exclude non-partners', 'fs' ),
			'filter_gov'             => __( 'loans without credit check', 'fs' ),
			'filter_age_20'          => __( 'lenders accepting age of 18-20', 'fs' ),
			'weekend_payout'         => __( 'lenders with weekend payout', 'fs' ),
			'step4'                  => __( 'Compare the best loans', 'fs' ),
			'newsletter'             => __( 'Let me know about great loan offers', 'fs' ),
			'newsletter_email'       => __( 'Enter your email', 'fs' ),
			'newsletter_name'        => __( 'Enter your name', 'fs' ),
			'display'                => __( 'Results', 'fs' ),
			'display_10'             => __( 'Top 10', 'fs' ),
			'display_20'             => __( 'Top 20', 'fs' ),
			'display_50'             => __( 'Top 50', 'fs' ),
			'display_100'            => __( 'Top 100', 'fs' ),
			'display_-1'             => __( 'All loans', 'fs' ),
			'sort'                   => __( 'Choose sorting', 'fs' ),
			'sort_company'           => __( 'Company name', 'fs' ),
			'sort_total'             => __( 'Total cost', 'fs' ),
			'sort_interest'          => __( 'Interest rate', 'fs' ),
			'sort_score'             => __( 'Best score', 'fs' ),
			'submit'                 => __( 'Find me the best loans', 'fs' ),
			'difference_text'        => __( 'Difference', 'fs' ),
			'total_loan_count'       => __( 'Results', 'fs' ),
			'tags'       			 => __( 'Tags', 'fs' ),
			'pdf_text'               => __( 'Download PDF', 'fs' ),
			'counter_header'         => __( 'Your comparison is completed', 'fs' ),
			'compared_help'          => __( 'We have helped people', 'fs' ),
			'compared_text'          => __( 'times to find', 'fs' ),
			'slider_type'            => __( 'loan', 'fs' ),
			'slider_type_plural'     => __( 'loans', 'fs' ),
			'better_loan_comparison' => __( 'Comparison service', 'fs' ),
			'your_loan_search'       => __( 'Your loan search', 'fs' ),
			'find_loan_for_you'      => __( 'Compare', 'fs' ),
			'about_page'             => __( 'About', 'fs' ),
			'charity_notice'         => __( '50 cents per loan to charity', 'fs' ),
			'loan_company'           => __( 'Loan Company', 'fs' ),
			'guide_1'           => __( 'Drag the sliders on the left to find your loan', 'fs' ),
			'guide_2'           => __( 'Customize your search with the filters (optional)', 'fs' ),
			'guide_3'           => __( 'Click', 'fs' ),
			'guide_4'           => __( 'to see the best companies', 'fs' ),
			'guide_5'           => __( 'Apply by clicking on the "View offer" button.', 'fs' ),
			'how_to_compare'           => __( 'How to compare', 'fs' ),
			'loan_quiz_text'           => __( 'Loan quiz - find out if you should take a loan first', 'fs' ),
			'total-cost'           => __( 'Total-cost', 'fs' ),
			'interest-rate'           => __( 'Interest-rate', 'fs' ),
			'best-rating'           => __( 'Best-rating', 'fs' ),
			'company-name'           => __( 'Company-name', 'fs' ),
			'show_additional_filters' => __( 'Show additional filters', 'fs' ),
			'hide_additional_filters' => __( 'Hide additional filters', 'fs' ),

		];
	}

	/**
	 * @param array $atts
	 */
	protected function processFilters( array $atts ) {
		if($this->sliderSettings->field( 'filter_non_affiliate' )!='1'){
			 unset( $this->filterList['non_affiliate'] );
		}
		
		if ( true == empty( $this->filters ) ) {
			$this->filters = $this->filterList;
		} else {
			if ( ! is_array( $this->filters ) ) {
				$this->filters = array_filter( explode( ',', $this->filters ) );
			}
			$this->filters = array_filter( $this->filters,
				function ( $filter ) {
					return in_array( $filter, $this->filterList );
				} );
		}
		$this->filters = array_unique( $this->filters );

		if ( false == empty( $this->excludeFilters ) ) {
			if ( ! is_array( $this->excludeFilters ) ) {
				
				$this->excludeFilters = array_filter( explode( ',', $this->excludeFilters ) );
			
			}

			
			$this->filters = array_filter( $this->filters,
				function ( $filter ) {
					return ! in_array( $filter, $this->excludeFilters );
				} );

			
		}
		$this->filters = array_values( $this->filters );

		foreach ( $this->filters as $filter ) {
			if ( isset( $atts[ 'filter_' . $filter ] ) ) {
				$this->$filter = sanitize_key( $atts[ 'filter_' . $filter ] );
			}
		}
	}

	/**
	 * Build pods query
	 * @return void
	 */
	protected function buildQuery() {

		static::initQuery();


		if ( ! is_array( $this->query['select'] ) ) {
			$this->query['select'] = [];
		}
		$select                 = self::generateLoanTotalSql( $this->amount, $this->period );
		$this->query['select']  = array_merge( $this->query['select'], $select );
		$this->query['join'][]  = 'LEFT JOIN `@wp_postmeta` AS `average-rating` ON `average-rating`.`post_id` = `t`.`ID`';
		$this->query['join'][]  = 'LEFT JOIN `@wp_postmeta` AS `total-ratings` ON `total-ratings`.`post_id` = `t`.`ID`';
		$this->query['where'][] = [
			'key'   => 'average-rating.meta_key',
			'value' => 'crfp-average-rating'
		];
		$this->query['where'][] = [
			'key'   => 'total-ratings.meta_key',
			'value' => 'crfp-total-ratings'
		];

		if ( false == empty( $this->interest_free ) && 1 ==
		                                               $this->interest_free
		) {
			$this->query['where'][] = [
				'relation' => 'OR',
				[

					'key'     => 'loan_datasets.d.interest_rate',
					'value'   => 0,
					'compare' => '='
				],
				[
					'key'     => 'loan_datasets.d.interest_rate',
					'compare' => 'NOT EXISTS'
				]
			];

			$this->query['where'][] = [
				'relation' => 'OR',
				[

					'key'     => 'loan_datasets.d.fee_flat',
					'value'   => 0,
					'compare' => '='
				],
				[
					'key'     => 'loan_datasets.d.fee_flat',
					'compare' => 'NOT EXISTS'
				]
			];

			$this->query['where'][] = [
				'relation' => 'OR',
				[

					'key'     => 'loan_datasets.d.fee_percent',
					'value'   => 0,
					'compare' => '='
				],
				[
					'key'     => 'loan_datasets.d.fee_percent',
					'compare' => 'NOT EXISTS'
				]
			];

			$this->query['where'][] = [
				'relation' => 'OR',
				[

					'key'     => 'loan_datasets.d.monthly_fee',
					'value'   => 0,
					'compare' => '='
				],
				[
					'key'     => 'loan_datasets.d.monthly_fee',
					'compare' => 'NOT EXISTS'
				]
			];


		}

		if ( false == empty( $this->tags ) ) {
			$new_tags = str_replace("%2C",",",$this->tags);
			$search_tags = explode(",", $this->tags);
			$where_tags = "";
			foreach($search_tags as $search_tag){
				if($where_tags!=''){$where_tags .= " OR "; }
				$where_tags .= "loan_datasets.d.loan_tags.slug = '" . $search_tag . "'";
				}	
				$this->query['where'][] = $where_tags;
			
		}

		if ( false == empty( $this->age_dropdown )
		) {
			$this->query['where'][] = [
				'key'     => 'd.minalder',
				'value'   => $this->age_dropdown,
				'compare' => '<=',
				'type'    => 'numeric',
			];
		}
		
		if ( false == empty( $this->filter_gov ) && 1 == $this->filter_gov
		) {
			$this->query['where'][] = 'credit_check.d.government IS NULL or credit_check.d.government = 0';
		}
		if ( false == empty( $this->filter_age_20 ) && 1 ==
		                                               $this->filter_age_20
		) {
			$this->query['where'][] = [
				'key'     => 'd.minalder',
				'value'   => 20,
				'compare' => '<',
				'type'    => 'numeric',
			];
		}
		if ( false == empty( $this->input_age ) && 0 !=
		                                               $this->input_age
		) {
			$this->query['where'][] = [
				'key'     => 'd.minalder',
				'value'   => $this->input_age,
				'compare' => '<',
				'type'    => 'numeric',
			];
		}
		if ( false == empty( $this->bad_history ) && 1 ==
		                                             $this->bad_history
		) {
			$this->query['where'][] = [
				'key'   => 'd.bad_history',
				'value' => '1',
			];
		}
		if ( false == empty( $this->non_affiliate ) && 1 ==
		                                             $this->non_affiliate
		) {
			$this->query['where'][] = [
				'key'   => 'd.ej_partner',
				'value' => '0',
			];
		}
		if ( false == empty( $this->weekend_payout ) && 1 ==
		                                                $this->weekend_payout
		) {
			$this->query['where'][] = [
				'key'   => 'd.helgutbetalning',
				'value' => '1',
			];
		}

		if ( false == empty( $this->loan_broker_filter_active ) && 1 ==
		                                                $this->loan_broker_filter_active
		) {

			$this->query['where'][] = [
				'key'   => 'd.loan_broker',
				'value' => '0',
			];
		}
		if ( false == empty( $this->loan_company ) && 1 ==
		                                                $this->loan_company
		) {
			$this->query['where'][] = [
				'key'     => 'company_type.slug',
				'compare' => '!=',
				'value'   => 'loan_company',
			];
		}
	}

	protected function initQuery() {
		$this->amount     = (int) $this->amount;
		$this->period     = (float) $this->period;
		$this->limit      = (int) $this->limit;
		$this->show_limit = (int) $this->show_limit;
		$this->input_age = 	(int) $this->input_age;

		switch ( $this->sort ) {
			case 'company-name':
				$sort = 'name ASC';
				break;
			case 'interest-rate':
				$sort = 'interest_rate ASC';
				break;
			case 'best-rating':
				$sort = 'CAST(average-rating.meta_value AS'
				        . ' DECIMAL(10,2)) DESC';
				break;
			case 'total-cost':
			default:
				$sort = "total_cost ASC";
				break;
		}

		$this->query = [
			'select'    => ! empty( $this->fields ) ? $this->fields : null,
			'limit'     => $this->limit,
			'orderby'   => $sort,
			'join'      => $this->joins,
			'calc_rows' => true,
			'groupby' => 't.post_name',
			'where'     => [
				[
					'key'     => 'loan_datasets.d.amount_range_minimum',
					'value'   => $this->amount,
					'compare' => '<=',
				],
				[
					'key'     => 'loan_datasets.d.amount_range_maximum',
					'value'   => $this->amount,
					'compare' => '>=',
				],
				"CAST(loan_datasets.d.period_range_minimum AS DECIMAL(12,4)) <= {$this->period}",
				"CAST(loan_datasets.d.period_range_maximum AS DECIMAL(12,4)) >= {$this->period}",
				'post_status'               => 'publish',
				'loan_datasets.post_status' => 'publish'
			]
		];
	}

	/**
	 * @param int $amount
	 * @param int $period
	 *
	 * @return array
	 */
	public static function generateLoanTotalSql( int $amount, int $period ) {
		return array_map( function ( string $line ) use ( $amount, $period ) {
			return str_replace( [ '{AMOUNT}', '{PERIOD}' ], [ $amount, $period ], $line );
		}, self::LOAN_TOTAL_SQL );
	}

	public static function increaseInstanceCounter() {
		self::$instance ++;
	}

	/**
	 * @return string
	 */
	public function getInstanceId(): string {
		return $this->instanceId;
	}

	/**
	 * @return \Pods
	 */
	public function getPod(): \Pods {
		return $this->pod;
	}

	/**
	 * @return float
	 */
	public function getAmount(): float {
		return $this->amount;
	}

	/**
	 * @param int $amount
	 */
	public function setAmount( int $amount ) {
		$this->amount = $amount;
	}

	/**
	 * @return int
	 */
	public function getPeriod(): int {
		return $this->period;
	}

	/**
	 * @param int $period
	 */
	public function setPeriod( int $period ) {
		$this->period = $period;
	}

	/**
	 * @return array
	 */
	public function getQuery(): array {
		return $this->query;
	}

	/**
	 * @return string
	 */
	public function getPostType(): string {
		return $this->postType;
	}

	/**
	 * @return string
	 */
	public function render(): string {
		ob_start();
		
		static::getResults();

		ob_start();
		static::renderCounters();
		$counters = ob_get_clean();

		static::header();
		static::renderNewsletter();
		static::table();
		echo $counters;
		static::footer();
		static::status();
		$deps = [
			'jquery-ui-widget',
			'jquery-ui-mouse',
			'jquery-ui-slider',
			'jquery-ui-tabs',
			'jquery-ui-dialog',
			'jquery-touch-punch',
			'jquery-effects-core',
		];
		foreach ( $deps as $script ) {
			wp_enqueue_script( $script );
		}
		wp_enqueue_script( 'dummy', Plugin::GetUri( 'js/dummy.js' ), [ 'jquery' ] );
		wp_add_inline_script( 'dummy', static::renderJs() );
		self::$instance ++;

		return ob_get_clean();
	}

	/**
	 * @return \Pods
	 */
	public function getResults() {

		$this->pod          = pods( $this->postType, $this->query );
		$this->total        = $this->pod->total();
		$this->totalResults = $this->pod->total_found();
		static::sort();
		
		return $this->pod;
	}

	protected function sort() {

	}

	protected function renderCounters() {
		if ( ! $this->isAjax ) {
			echo <<<HTML
</div></div></div></div></div>
<div class="boxWrap counters hidden">
<div class="wrap counterWrapper">
<div class="w30 left">
<i class="comparison"></i>
</div>
<div class="w70 right">
    <div class="counterWrapper">
        <h2>{$this->htmlLabels['counter_header']}</h2>

        <div class="counter">
HTML;
		}
		if ( ( ! $this->isAjax && get_query_var( 'query' ) ) || $this->isAjax ) {
			static::renderCounterItems();
		}
		if ( ! $this->isAjax ) {
			echo <<<HTML
		</div>
    </div>
</div></div></div>
HTML;
        }
}

	/**
	 *
	 */
	protected function renderCounterItems() {
		static::renderDifference();
		static::renderTotalCount();
	}

	/**
	 * @return mixed
	 */
	abstract protected function renderDifference();

	/**
	 *
	 */
	protected function renderTotalCount() {
		echo <<<HTML
<div class="block-circle">

    <p class="block-text">{$this->htmlLabels['total_loan_count']}</p>

    <p class="block-count"><span class="counter">{$this->total}</span>/{$this->totalResults}</p>
</div>
HTML;

	}

	/**
	 * @return void
	 */
	protected function renderSharing() {
		$params = [];
		array_walk(
			$this->shareableParamsList,
			function ( $item ) use ( &$slider, &$params ) {
				$params[ 'param_' . $item ] = $this->$item;
			}
		);
		array_walk(
			$this->filters,
			function ( $item ) use ( &$slider, &$params ) {
				$params[ 'filter_' . $item ] = $this->$item;
			}
		);
		$query = str_replace( '=', '/', http_build_query( $params, null, '/' ) );

		echo Social::render( [ 'id' => $this->post_id, 'after' => 'query/' . $query ] );
	}

	/**
	 *
	 */
	protected function header() {
        $limits_sort = '';
        $steps     = array_flip( $this->steps );
        if($this->sliderSettings->field( 'enable_age_dropdown' )!='1'){
			unset( $steps['age_dropdown'] );
		}
        if ( !$this->tagsEnabled && isset( $steps['loan_tags'] )) {
        	unset( $steps['loan_tags'] );
        }
        if ( isset( $steps['limits_sort'] ) ) {
            ob_start();
            $this->_runStep( 'limits_sort' );
            $limits_sort = ob_get_clean();
            unset( $steps['limits_sort'] );
            $this->steps = array_keys( $steps );
        }
		$form      = static::form();
		$preHeader = static::preHeader();
		$afterHeader = static::afterHeader();
		$loanQuiz = static::loanQuiz();
		$counter   = $this->sliderSettings->field( $this->compareCounterSetting );
		$title = get_the_title();
		echo <<<HTML
<style type="text/css">
    .ui-slider {
        opacity: 0;
    }
</style>
<div class="wrap w100">
<div class="col-wide">
<div class="blogBox widget widget_text loan-search">
            $preHeader
            <form action="" method="post">
                <div class="fl_l w_70 slider_{$this->id}">
				<div class="slider-form">
                $form
				</div>
				<div class="slider-description">
<h3>{$this->htmlLabels['how_to_compare']}</h3>
<ol>
<li>{$this->htmlLabels['guide_1']}.</li>
<li>{$this->htmlLabels['guide_2']}.</li>
<li>{$this->htmlLabels['guide_3']} "{$this->htmlLabels['submit']}" {$this->htmlLabels['guide_4']}.</li>
<li>{$this->htmlLabels['guide_5']}</li>
</ol>
				</div>
            </form>
</div>
$loanQuiz
$afterHeader
</div></div>
</div>
</div>
</div>
</div>
</div>
<div class="boxWrap compare-results slider-results">
<div class="wrap">
<div class="entrySide">
<h2 class="secondtitle">
HTML;
if ( ! empty( $this->title ) ) {
			echo <<<HTML
	{$this->title}		
HTML;
		}else{
	echo <<<HTML
	{$this->htmlLabels['find_loan_for_you']} {$this->htmlLabels['slider_type_plural']}
HTML;
		}
	echo <<<HTML
	</h2>
	<i class="arrow"></i>$limits_sort
HTML;
	}

	/**
	 * @return mixed
	 */
	protected function form() {
		ob_start();
		echo <<<HTML
		<div id="{$this->instanceId}_form">
HTML;
		$this->runSteps();
		echo <<<HTML
		</div>
HTML;

		return ob_get_clean();
	}

	/**
	 *
	 */
	public function runSteps() {
		foreach ( $this->steps as $step ) {
			$this->_runStep( $step );
		}
	}

	/**
	 * @param string $step
	 */
	private function _runStep( string $step ) {
		$step   = 'step' . ucfirst( $step );
		$header = $step . 'Header';
		$footer = $step . 'Footer';
		if ( method_exists( $this, $header ) ) {
			static::$header();
		} else {
			static::stepHeader( $step );
		}
		if ( method_exists( $this, $step ) ) {
			static::$step();
		}
		if ( method_exists( $this, $footer ) ) {
			static::$footer();
		} else {
			static::stepFooter( $step );
		}
	}

	/**
	 * @param string $step
	 */
	protected function stepHeader( string $step ) {
		echo <<<HTML
<div class="step $step">
HTML;
	}

	/**
	 * @param string $step
	 */
	protected function stepFooter( string $step ) {
		if(get_query_var( 'query' )){
			echo '<script>scrollToResults();</script>';
		}
		echo <<<HTML
</div>
HTML;
	}

	/**
	 * @return string
	 */
	protected function preHeader(): string {
		return '';
	}

	protected function afterHeader(): string {
		return '';
	}

	protected function loanQuiz(): string {
		return '';
	}

	/**
	 * @internal param bool $empty
	 *
	 * @internal param bool $wrapper
	 */
	protected function table() {
		global $wp_query;
		$show_all_ui = ! is_null( $this->show_limit ) && $this->pod->total() > $this->show_limit;
		if ( $show_all_ui && empty( $this->show_all ) && ! $this->isAjax ) {
			$this->pod->rows        = array_slice( $this->pod->rows, 0, $this->show_limit );
			$this->pod->total       = $this->show_limit;
			$this->pod->total_found = $this->show_limit;
		}
		if ( ! $this->isAjax ) {
			echo <<<HTML
		  <div id="{$this->instanceId}" class="tw-bs table_cont">
HTML;
		}

		$old_in_the_loop       = $wp_query->in_the_loop;
		$wp_query->in_the_loop = true;
		$class                 = '\Financer\FilterSlider\Table\\' . $this->tableClass;
		/* @var $class \Financer\FilterSlider\Interfaces\TableInterface */
		$class::build( $this->pod, $this );
		$wp_query->in_the_loop = $old_in_the_loop;
		if ( $show_all_ui ):
			if ( ( $this->isAjax && ! empty( $this->show_all ) ) || ( ! $this->isAjax && empty( $this->show_all ) ) ):
				if ( empty( $this->show_all ) ):
					?>
                    <div class="show-all-holder"><a class="show-all"><?php _e( 'Show All', 'fs' ); ?></a></div>
				<?php else: ?>
                    <div class="show-all-holder"><a class="show-all"><?php _e( 'Collapse Items', 'fs' ); ?></a></div>
					<?php
				endif;
			endif;
		endif;
	}

	/**
	 *
	 */
	public function footer() {
		echo <<<HTML
		</div></div></div></div>
		<div class="blueBar hide"></div>
HTML;
		if ( is_front_page() ) {
			echo <<<HTML
<div class="wrap">
    <div class="col-wide">
        <div class="entry">
HTML;
		}
	}

	/**
	 *
	 */
	protected function status() {
		$label        = __( 'Loading results according to your criteria', 'fs' );
		$loader_image = home_url( '/graph/loader.gif' );
		echo <<<HTML
	<div class="status_bar">
	   <p>
		  $label<img class="loader"
			 src="$loader_image" width="248" height="248">
		  </p>
	   </div>
HTML;
	}

	/**
	 *
	 */
	public function renderJs(): string {
		$output = static::renderJsMaps();
		$jsInit = static::renderJsInit();
		$jsData = static::renderJsData();
		$jsAjax = static::renderJsAjax();
		/**
		 * @var string $output
		 */
		/**
		 * @var string $jsInit
		 */
		/**
		 * @var string $jsData
		 */
		/**
		 * @var string $jsAjax
		 */
		$output .= <<<JS
	   (function () {
		  (function init() {
			 if (typeof jQuery !== 'undefined' && typeof jQuery.ui !== 'undefined') {
				$jsInit;
				function generate_table_$this->instanceId() {
					$jsData;
					$jsAjax;
				}
			 }
			 else {
				setTimeout(init, 50);
			 }
		  })();

	   })();
JS;

		return $output;
	}

	/**
	 * @return string
	 */
	public function renderJsMaps(): string {
		$output = '';
		foreach ( static::generateJsMaps() as $name => $map ) {
			$list = [];
			foreach ( $map as $value => $text ) {
				$list[] = [
					'text'  => $text,
					'value' => $value,
				];
			}
			$jsonList = wp_json_encode( $list );
			$output .= <<<JS
	var $name = $jsonList;
JS;
		}

		return $output;
	}

	/**
	 * @param array $params
	 *
	 * @return array
	 */
	public function generateJsMaps( $params = [] ): array {
		$params = array_merge_recursive(
			[
				'select' => [
					'MAX(loan_datasets.d.amount_range_maximum) AS loan_amount_range_maximum',
					'MIN(loan_datasets.d.amount_range_minimum) AS loan_amount_range_minimum',
					'MAX(CAST(loan_datasets.d.period_range_maximum AS DECIMAL(12,4))) AS loan_period_range_maximum',
					'MIN(CAST(loan_datasets.d.period_range_minimum AS DECIMAL(12,4))) AS loan_period_range_minimum',
				],
				'limit'  => - 1,
				'where'  => [
					[
						'key'   => 'post_status',
						'value' => 'publish',
					],
				],
			], $params
		);

		if ( ! empty( $this->maxAmount ) ) {
			$params['where'][] = [
				'key'     => 'loan_datasets.d.amount_range_maximum',
				'value'   => $this->maxAmount,
				'compare' => '<=',
			];
		}
		if ( ! empty( $this->maxPeriod ) ) {
			$params['where'][] = "CAST(loan_datasets.d.period_range_maximum AS DECIMAL(12, 4)) <= {$this->maxPeriod}";
		}

		$pod     = pods( $this->postType, $params );
		$periods = [];
		$amounts = [];
		if ( $pod->fetch() ) {
			$amount_min = (int) $pod->field( 'loan_amount_range_minimum' );
			$amount_max = (int) $pod->field( 'loan_amount_range_maximum' );

			$amounts_list = $this->amounts;
			if ( empty( $amounts_list ) ) {
				$amounts_list = $this->sliderSettings->field( 'loan_amounts' );
			}
			if ( ! is_array( $amounts_list ) ) {
				$amounts_list = array_filter( array_map( 'absint', array_map( 'trim', explode( ',', $amounts_list ) ) ) );
			}
			$amounts_list = array_filter( array_map( 'absint', $amounts_list ), function ( $amount ) use ( $amount_min, $amount_max ): bool {
				return $amount <= ( $this->maxAmount ? $this->maxAmount : $amount_max ) && $amount >= $amount_min;
			} );
			foreach ( $amounts_list as $amount ) {
				$amounts[ $amount ] = Util::moneyFormat( $amount ) . ' ' . __( 'usd', 'fs' );
			}
			$period_min = (int) $pod->field( 'loan_period_range_minimum' );
			$period_max = (int) $pod->field( 'loan_period_range_maximum' );

			$periods_list = $this->days;

			if ( empty( $periods_list ) ) {
				$periods_list = $this->sliderSettings->field( 'loan_periods' );
			}
			if ( ! is_array( $periods_list ) ) {
				$periods_list = array_filter( array_map( 'absint', array_map( 'trim', explode( ',', $periods_list ) ) ) );
			}
			$periods_list = array_filter( array_map( 'absint', $periods_list ), function ( $period ) use ( $period_min, $period_max ): bool {
				return $period <= ( ! empty( $this->maxPeriod ) ? $this->maxPeriod : $period_max ) && $period >= $period_min;
			} );
			foreach ( $periods_list as $period ) {
				$periods[ $period ] = Util::getPeriod( $period );
			}
			ksort( $periods );
			ksort( $amounts );
		}

		return [
			$this->instanceId . '_amountMap' => $amounts,
			$this->instanceId . '_periodMap' => $periods,
		];
	}

	/**
	 * @return string
	 *
	 */
	protected function renderJsInit(): string {
		Social::registerJs();
		$sliderInitJs = static::renderSliderInitJs();
		$reportJs     = self::renderReportJs();
		$detailsJs    = self::renderDetailsJs();
		$preloaderJs  = self::renderPreloaderJs();
		$jsData = static::renderJsData();
		$hide_text    = __( 'Collapse Items', 'fs' );
		$show_text    = __( 'Show All', 'fs' );
		$limitJs      = '';
		$url = get_site_url( null, '/wp-admin/admin-ajax.php?' . $this->getParamString() );
		/**
		 * @var int $period
		 */

		/**
		 * @var int $amount
		 */

		if ( ! empty( $this->show_limit ) ) {
			$limitJs = <<<JS
$('#{$this->instanceId} .item-table .item-row:not(.details):not(.tag-example):gt({$this->show_limit})').hide();
$('#{$this->instanceId} ~ .msg').hide();
$('#{$this->instanceId}').on('click', '.show-all',function () {
	
    var self = this;
    if ($('#{$this->instanceId}').data('show_all')) {

        $('#{$this->instanceId} .item-table .item-row:not(.details):not(.tag-example):gt({$this->show_limit})').fadeToggle(400, 'swing', function () {

        	//$('.tag-example').closest('.item-row').text();
            if ($('#{$this->instanceId} .item-table .item-row:not(.details):not(.tag-example):gt({$this->show_limit})').is(':visible')) {
                $(self).text('{$hide_text}');

                $('.tag-example').each(function( index ) {
	                	if($(this).prev('.item-row').is(':visible')){
	                	$(this).show();
	                	}else{
	                		$(this).hide();
	                	}
       				});
                
                 $('.navigation-content').removeClass('stick');
    			scrollTableContents();

                
            }
            else {
                var detailItems = $('#{$this->instanceId} .item-table .item-row:not(.details):not(.tag-example):gt({$this->show_limit})').next();
                detailItems.removeClass('expanded');
                detailItems.prev().find('.fa').removeClass('fa-minus').addClass('fa-plus');
                $(self).text('{$show_text}');
                 $('.tag-example').each(function( index ) {
	                	if($(this).prev('.item-row').is(':visible')){
	                	$(this).show();
	                	}else{
	                		$(this).hide();
	                	}
       				});

       				 $('.navigation-content').removeClass('stick');
    									scrollTableContents();
            }
        });
        $('#{$this->instanceId} .msg').fadeToggle();


    }
    else{
        $jsData;
        data['param_show_all'] = 1;
        $.ajax({
        url: '{$url}',
        type: "POST",
        data: data,
        beforeSend: function () {
            $(".status_bar").fadeIn();
        },
        success: function (data) {
                $(".status_bar").fadeOut();
                $('#{$this->instanceId}').slideUp(1000, function () {
                    var counterWrapper = $('.counterWrapper');
                    counterWrapper.parent().removeClass('hidden');
                    var counters = counterWrapper
                        .find('div.counter')
                        .html(data.counters)
                        .find('span.counter')
                        .parent();
                    counters.hide();
                    $('#{$this->instanceId}')
                        .html(data.html)
                        .slideDown(1000, function () {
                            if (1000 > $(window).width()) {
                                $('html, body').animate({scrollTop: $(this).offset().top})
                            }
                            counters
                                .show()
                                .children('span')
                                .counterUp({
                                    delay: 1,
                                    time: 500
                                }).fadeIn();
                                //
                                $('.sort-item').each(function( index ) {
							        $(this).nextUntil('.sort-item').andSelf().wrapAll('<div class="sortthis" />');
							        }).promise()
							        .done( function() {
							            
							            change_sort();
							            
							            $('.navigation-content').removeClass('stick');
    									scrollTableContents();
							    });
							    //
                        });
                    $('.blueBar').fadeIn();
                    scrollToResults();
                    


                });
                $('#{$this->instanceId}').data('show_all', true);
        }
    });
    }
})



JS;
		}

		return <<<JS
jQuery(function ($) {
    $limitJs;
    $sliderInitJs;


    $('#{$this->instanceId}_form .get_results').click(function (e) {
        generate_table_$this->instanceId();
        $('#{$this->instanceId} .show-all-holder').remove();
        $('.counterWrapper').parent().addClass('hidden');
        var data = $('#{$this->instanceId}_form :input').serializeArray();
        if($('.current').attr('data-toggle')){
        data.push({name: "param_sort", value: $('.current').attr('data-toggle')});
    	}


        $('#{$this->instanceId}_form .ui-slider').each(function () {
            var name = $(this).data('name');
            var value = $(this).slider('value');
            var value_object = window['{$this->instanceId}_' + name.replace('param_', '') + 'Map'][value];
            data.push({
                name: name,
                value: value_object ? value_object.value : 0
            });
        });


		
        data = $.param(data);
        var url = window.location.href;
        var urlParts = window.location.href.split('query');
        if (urlParts.length > 1) {
            url = urlParts.shift();
        }
        url += 'query/' + data.replace(/[\&\=]/g, '/');
        history.replaceState(null, null, url);
        

        e.preventDefault();
        return false;
    });
    $reportJs;
    $detailsJs;
});
(function ($) {
    $(window).load(function () {
        var counterWrapper = $('.counterWrapper');
        var counters = counterWrapper
            .find('div.counter')
             .find('span.counter')
             .parent();
        if (counters.children().length) {
            counterWrapper.parent().removeClass('hidden');
            counters.hide();
            counters
                .show()
                .children('span')
                .counterUp({
                    delay: 1,
                    time: 500
                }).fadeIn(function(){
                	$('.navigation-content').removeClass('stick');
    									scrollTableContents();
                });
        }

    });
})(jQuery);

$preloaderJs
JS;
	}

	/**
	 * @return string
	 */
	public function renderSliderInitJs() {
		return <<<JS
$('#{$this->instanceId}_form .ui-slider').each(function () {
    var self = $(this);
    var defaultValue = parseFloat($(this).data('defaultValue'));

    var name = $(this).data('name').replace('param_', '');
    var map = window['{$this->instanceId}_' + name + 'Map'];
    var display = $(this).data('display');

    map.every(function (v, i) {
        if (parseFloat(v.value) == parseFloat(defaultValue)) {
            defaultValue = map.length > 0 ? i : 0;
            return false;
            
        }
        else if (i == map.length - 1) {
            defaultValue = 0;
        }
        return true;
    });

    $(this).slider({
        range: 'min',
        min: 0,
        max: map.length - 1,
        value: defaultValue,
        slide: function (event, ui) {
            $(self).parent().parent().find('.' + display).html(map[ui.value].text);

        },
        create: function (event, ui) {
            $(event.target).fadeTo(1000, 1);
        }
    });
    $(this).prevAll('.slider_cont').find('.minus_arrow').click(function () {
        self.slider('value', self.slider('value') - self.slider("option", "step"));
        if (map[self.slider('value')]) {
            $(self).parent().parent().find('.' + display).html(map[self.slider('value')].text);
        }
        return false;
    });
    $(this).prevAll('.slider_cont').find('.plus_arrow').click(function () {
        self.slider('value', self.slider('value') + self.slider("option", "step"));
        if (map[self.slider('value')]) {
            $(self).parent().parent().find('.' + display).html(map[self.slider('value')].text);
        }
        return false;
    });
    if (map[defaultValue]) {
        $(self).parent().parent().find('.' + display).html(map[defaultValue].text);
    } 
});
JS;
	}

	/**
	 * @return string
	 */
	public static function renderReportJs(): string {
		$reportConfirmTitle   = __( 'Wrong data', 'fs' );
		$reportConfirmMessage = __( 'Are you sure you wish to report this data?', 'fs' );
		$reportSuccessTitle   = __( 'Data reported', 'fs' );
		$reportSuccess        = __( 'Thank you for your contribution', 'fs' );
		$yesBtn               = __( 'Yes', 'fs' );
		$noBtn                = __( 'No', 'fs' );
		$closeBtn             = __( 'Close', 'fs' );
		$reportUrl            = get_site_url( null, '/wp-admin/admin-ajax.php' );
		$errorJs              = self::renderCacheErrorJs();
		wp_enqueue_script( 'jquery-ui-dialog' );

		return <<<JS
$(document).on('click', '.table_cont .report', function () {
    var self = this;
    $('<div></div>', {
        "class": 'reportModal'
    }).dialog({
        modal: true,
        title: '{$reportConfirmTitle}',
        open: function () {
            $(this).html('{$reportConfirmMessage}');
        },
        buttons: {
            "$noBtn": function () {
                $(this).dialog("close");
            },
            "$yesBtn": function () {
                $(this).dialog("close");
                $.ajax({
                    url: '{$reportUrl}',
                    type: "POST",
                    data: {
                        action: "filter_slider_report",
                        id: $(self).closest('.item-row').data('id')
                    },
                    success: function (data) {

                        if (!data.length) {
                            $('<div></div>', {
                                "class": 'reportModal'
                            }).dialog({
                                modal: true,
                                title: '{$reportSuccessTitle}',
                                open: function () {
                                    $(this).html('{$reportSuccess}');
                                },
                                buttons: {
                                    "$closeBtn": function () {
                                        $(this).dialog("close");
                                    }
                                }
                            });
                        }
                        else {
                            $errorJs
                        }
                    }
                })
            }
        }
    });
    return false;
});
JS;
	}

	/**
	 * @return string
	 */
	public static function renderCacheErrorJs(): string {
		$clearCacheUrl = get_site_url( null, '/wp-admin/admin-ajax.php' );
		$errorTitle    = __( 'Something went wrong', 'fs' );
		$countDownText = __( 'Reloading the page in %d seconds', 'fs' );

		return <<<JS
$.post('$clearCacheUrl', {
                        'action': 'purge_cache'
                    },
                    function () {
	                    $(".status_bar").fadeOut();
	                    var text = '$countDownText';
	                    var time = 3;
	                    var modal = $('<div></div>');
	                    modal.dialog({
                            modal: true,
                            closeOnEscape: false,
                            title: '{$errorTitle}',
                            create: function () {
		                    $(this).dialog('instance').uiDialogTitlebarClose.remove();
	                    },
                            open: function () {
		                    $(this).text(text.replace('%d', time));
	                    }
                        });
                        (function tick() {
	                        if (-1 == time) {
		                        modal.dialog('instance').uiDialog.fadeTo('slow', 0, function () {
			                        modal.dialog('close');
			                        window.location.reload();
		                        });
	                        }
	                        else {
		                        modal.text(text.replace('%d', time));
		                        setTimeout(tick, 1000);
		                        time--;
	                        }

                        })();
                    }
                );
JS;
	}

	/**
	 * @return string
	 */
	public static function renderDetailsJs() {
		return <<<JS
$(document).on('click', '.table_cont .fa', function () {
        var detail = $(this).closest('.item-row').next('.details');
        detail.toggleClass('expanded');
        $(this).toggleClass('fa-plus', !detail.hasClass('expanded'));
        $(this).toggleClass('fa-minus', detail.hasClass('expanded'));
    });
JS;

	}

	/**
	 * @return string
	 */
	public function renderPreloaderJs(): string {
		return <<<JS
jQuery(window).load(function () {
    (function check() {
        if (!jQuery('#{$this->instanceId}_form .get_results').is(':visible')) {
            jQuery('#{$this->instanceId}_form .get_results').fadeIn();
            setTimeout(check)
        }
    })();
});
JS;

	}

	/**
	 * @return string
	 *
	 */
	public function renderJsData(): string {
		return <<<JS
		var data = {};
(function ($) {
    var dataArray = $('#{$this->instanceId}_form :input').serializeArray();
    if($('.current').attr('data-toggle')){
    dataArray.push({name: "param_sort", value: $('.current').attr('data-toggle')});
	}
    var form = jQuery('#{$this->instanceId}_form');
    form.find('.ui-slider').each(function () {
        var name = $(this).data('name');
        var value = $(this).slider('value');
        var value_object = window['{$this->instanceId}_' + name.replace('param_', '') + 'Map'][value];
        dataArray.push({
            name: name,
            value: value_object ? value_object.value : 0
        });
    });
    dataArray.forEach(function (item) {
        data[item.name] = item.value;
    });

    data['action'] = 'filter_slider_{$this->id}';
    data['email'] = form.find('.email').val();
    data['name'] = form.find('.name').val();
})(jQuery);

JS;
	}

	/**
	 * @return string
	 * @internal param Slider $slider
	 *
	 */
	public function getParamString(): string {
		$params = [];
		array_walk(
			$this->paramsList,
			function ( $item ) use ( &$slider, &$params ) {
				$params[ 'param_' . $item ] = $this->$item;
			}
		);
		$params['post_id'] = get_the_ID();
		if ( $this->debug ) {
			$params['pods_debug_sql'] = 1;
		}

		return http_build_query( $params );
	}

	/**
	 * @return string
	 * @internal param $slider
	 *
	 */
	protected function renderJsAjax(): string {
		$url     = get_site_url( null, '/wp-admin/admin-ajax.php?' . $this->getParamString() );
		$error   = __( 'Error - try another browser !:', 'fs' );
		$errorJs = self::renderCacheErrorJs();
		$admin   = wp_json_encode( current_user_can( 'administrator' ) );
		$js      = <<<JS
(function ($) {
    $.ajax({
        url: '{$url}',
        type: "POST",
        data: data,
        beforeSend: function () {
            $(".status_bar").fadeIn();
        },
        success: function (data) {
            if (typeof data == 'object') {

                $(".status_bar").fadeOut();
                $('#{$this->instanceId}').slideUp(1000, function () {
                    $(".steg4 #newsletter_status").remove();
                    $('#{$this->instanceId} ~ .msg').hide().removeClass('hidden').fadeIn();
                    if (data.newsletter !== false) {
                        $('<p/>', {
                            text: data.newsletter.message,
                            id: 'newsletter_status'
                        }).hide().addClass(data.newsletter.error ? 'error' : 'success').appendTo($(".steg4")).fadeIn().delay(10000).fadeOut();
                    }
                    var counterWrapper = $('.counterWrapper');
                    counterWrapper.parent().removeClass('hidden');
                    var counters = counterWrapper
                        .find('div.counter')
                        .html(data.counters)
                        .find('span.counter')
                        .parent();
                    counters.hide();

                    $(this).siblings('.entry.dashView').find('.sharing').html(data.sharing);
                   
                    $('.blueBar .compare_counter .block-count').html(data.request_counter);
                    $('.stepLoan_tags').html(data.tags);

                    $('#{$this->instanceId}')
                        .html(data.html)
                        .slideDown(1000, function () {
                            if (1000 > $(window).width()) {
                                $('html, body').animate({scrollTop: $(this).offset().top})
                            }
                            counters
                                .show()
                                .children('span')
                                .counterUp({
                                    delay: 1,
                                    time: 500
                                }).fadeIn();

                                 //
                                $('.sort-item').each(function( index ) {
							        $(this).nextUntil('.sort-item').andSelf().wrapAll('<div class="sortthis" />');
							        }).promise()
							        .done( function() {
							            
							            change_sort();
							            
							            $('.navigation-content').removeClass('stick');
    									scrollTableContents();
        								toggle_loan_tags();
							    });
							    //
                        });
                    $('.blueBar').fadeIn();
                    $('.found_total').html($('.sort-item').length);
                    scrollToResults();

                });
            }
            else {
                $errorJs
            }
        },
        error: function (msg) {
            if ($admin) {
                alert("$error " + msg);
            } else {
                generate_table_{$this->instanceId}();
            }
        }
    });
})(jQuery);
JS;

		return $js;
	}

	/**
	 * @param bool $array
	 *
	 * @return string
	 */
	public function getFilters( $array = false ) {
		return $array ? $this->filters : ( is_array( $this->filters ) ? implode( ',', $this->filters ) : $this->filters );
	}

	/**
	 *
	 */
	public function renderAjax() {
		header( 'Content-Type: application/json' );
		$newsletter = static::_processEmailSubscription();
		static::getResults();
		ob_start();

		$this->stepLoan_tags();
		if($this->sliderSettings->field( 'enable_age_dropdown' )=='1'){
		$this->stepAge_dropdown();
		}
		$tags = ob_get_clean();

		ob_start();
		static::renderCounters();
		$counters = ob_get_clean();

		ob_start();
		
		static::table();
		$html = ob_get_clean();
		$this->pod->reset();

		ob_start();
		static::renderSharing();
		$sharing        = ob_get_clean();
		$requestCounter = (int) $this->sliderSettings->field( $this->compareCounterSetting );
		$json           = [
			'html'            => $html,
			'counters'        => $counters,
			'request_counter' => $requestCounter,
			'newsletter'      => $newsletter,
			'sharing'         => $sharing,
			'tags'         	  => $tags,
			'age_dropdown'    => $age_dropdown,
		];
		if ( empty( $this->show_all ) ) {
			$this->sliderSettings->save( $this->compareCounterSetting, $requestCounter + 1 );
		}
		echo wp_json_encode( $json );
	}

	/**
	 * @return bool
	 */
	private function _processEmailSubscription() {
		global $email_newsletter, $wpdb;
		if ( ! empty( $_POST['email'] ) && is_email( $_POST['email'] ) ) {
			$_REQUEST['e_newsletter_email'] = sanitize_email( $_POST['email'] );
			$_REQUEST['e_newsletter_name']  = sanitize_text_field( $_POST['name'] );
			if ( false == empty( $this->newsletterGroup ) ) {
				$member = $email_newsletter->get_member_by_email( $_POST['email'] );
				$group  = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM {$email_newsletter->tb_prefix}enewsletter_groups WHERE group_name = %s", $this->newsletterGroup ), "ARRAY_A" );
				if ( false == empty( $member ) ) {
					if ( empty( $member['unsubscribe_code'] ) ) {
						$status = [
							'error'   => true,
							'message' => __( 'Please check your email for a confirmation email before subscribing to another list',
								'email-newsletter' )
						];
					} else {
						$email_newsletter->add_members_to_groups( [ $member['member_id'] ], [ $group['group_id'] ] );
						$status = [ 'action' => 'new_subscribed', 'error' => false, 'message' => __( 'You have been successfully subscribed!', 'email-newsletter' ) ];
					}
				} else {
					$_REQUEST['e_newsletter_groups_id'] = [ $group['group_id'] ];
					$status                             = $email_newsletter->new_subscribe();
				}
			} else {
				$status = $email_newsletter->add_member(
					[
						'email'        => sanitize_email( $_POST['email'] ),
						'member_fname' => sanitize_text_field( $_POST['name'] ),
					],
					1
				);
			}

			return $status;
		}

		return false;
	}

	/**
	 * @return mixed
	 */
	public function getNewsletterGroup(): string {
		return $this->newsletterGroup;
	}

	/**
	 * @param $var
	 *
	 * @return null|string
	 */
	public function __get( $var ): string {
		$vars = get_class_vars( get_class( $this ) );
		foreach ( $vars as $key => $value ) {
			if ( strtolower( $var ) == strtolower( $key ) ) {
				return $this->$key;
				break;
			}
		}

		return '';
	}

	/**
	 * @param $var
	 * @param $value
	 *
	 * @return void
	 */
	public function __set( $var, $value ) {
		$vars = array_keys( get_class_vars( get_class( $this ) ) );
		foreach ( $vars as $prop ) {
			if ( strtolower( $var ) == strtolower( $prop ) ) {
				$this->$prop = $value;
				break;
			}
		}
	}

	/**
	 *
	 */
	public function renderPdf() {
		$this->isPdf = true;
		static::getResults();
		ob_start();
		$class = '\Financer\FilterSlider\Table\\' . $this->tableClass;
		$class::build( $this->pod, $this );
		$html = ob_get_clean();
		$pdf = new Dompdf();
		$css = file_get_contents( Plugin::GetDir( 'pdf.css' ) );
		$pdf->loadHtml( <<<HTML
<style type="text/css">
{$css}
</style>

HTML
		                .

		                $html );
		$pdf->render();
		$pdf->stream( 'financer_com_' . $this->id . '_' . date( 'm-d-Y' ) . '.pdf' );
	}

	/**
	 * @return boolean
	 */
	public function isPdf(): bool {
		return $this->isPdf;
	}

	/**
	 * @return string
	 */
	public function getId(): string {
		return $this->id;
	}

	/**
	 * @return bool|\Pods
	 */
	public function getDebug(): bool {
		return $this->debug;
	}

	/**
	 * @param array $steps
	 */
	public function setSteps( array $steps ) {
		$this->steps = $steps;
	}

	/**
	 *
	 */
	protected function renderNewsletter(){
		/*newletter*/
	}
	protected function stepLimits_sort() {

		/*limits and sort only present in GenericLoan*/

	}
	protected function stepAge_dropdown() {
		/*age dropdown for genericloan only*/
	}

	protected function stepLoan_tags() {
		
		/*loan tags dropdown*/
					if ( $this->tagsEnabled ) {
			
					$terms = get_terms( array(
					    'taxonomy' => 'loan_tags',
					    'hide_empty' => false,
					) );

				 	if($terms){ 
					//
					echo <<<HTML
					<div class="tagsdiv">
				            {$this->tagsEnabled}:
				            <select class="filter_nav" name="param_tags" id="param_tags">
				            <option value="">---</option>
HTML;
						foreach ( $terms as $term ) {
							//$selected = selected( $this->tags, $term );
							echo <<<HTML
						    <option value="$term->slug">{$term->name}</option>
HTML;
						}
						echo <<<HTML
				            </select>

				        </div>
HTML;
					}

				}
			/**/
	}
	protected function stepSubmit() {


		echo <<<HTML
<a href class="get_results button medium compare">{$this->htmlLabels['submit']}</a>
HTML;
	}
	protected function stepAmountHeader() {
		echo <<<HTML
<div class="step amount">
HTML;
	}

	/**
	 *
	 */
	protected function stepAmount() {
		$link = get_the_permalink();
		
		echo '
    <label class="fl_l_m_10"> ' . $this->htmlLabels['amount'] . ' </label>
    <div class="slider_cont">
        <a href="'. $link . '" class="minus_arrow">-</a>
        <span class="fl_l_m_15 amount_display"></span>
        <a href="'. $link . '" class="plus_arrow">+</a>
    </div>
    <div class="ui-slider" data-name="param_amount" data-display="amount_display" data-default-value="'. $this->amount . '"></div>
';

	}

	/**
	 *
	 */
	protected function stepAge() {
		$link = get_the_permalink();
		echo <<<HTML
    <label class="fl_l_m_10">{$this->htmlLabels['age']}: </label>
    <div class="slider_cont2">
    <input type="number" name="param_input_age" min="1" max="99" value="">
    </div>
HTML;

	}
	/**
	 *
	 */
	protected function stepPeriod() {
		$link = get_the_permalink();
		echo <<<HTML
    <label class="fl_l_m_10">{$this->htmlLabels['period']} </label>
    <div class="slider_cont">
        <a href="$link" class="minus_arrow">-</a>
        <span class="fl_l_m_15 period_display"></span>
        <a href="$link" class="plus_arrow">+</a>
    </div>
    <div class="ui-slider" data-name="param_period" data-display="period_display"  data-default-value="{$this->period}"></div>
HTML;

	}

	/**
	 * @return void
	 */
	protected function stepFilters() {
		echo <<<HTML
		<div class="additional_filters show" style="display: none;"><a href="javascript:void(0)" class="show_add_filters">{$this->htmlLabels['show_additional_filters']}</a></div>

		<div class="additional_filters hide" style="display: none;"><a href="javascript:void(0)" class="show_add_filters">{$this->htmlLabels['hide_additional_filters']}</a></div>
	   <label class="fl_l_m_10" style="display: none;">{$this->htmlLabels['filters']} </label>
HTML;
		foreach ( $this->filters as $filter ) {
			$checked = checked( true, (bool) $this->$filter || in_array( $filter, $this->filtersEnabled ), false );
			echo <<<HTML
	   <div class="checkboxFive" style="display: none;">
		  <input type="checkbox" value="1" id="{$this->instanceId}_$filter" name="filter_$filter" class="$filter"$checked>
		  <label for="{$this->instanceId}_$filter">{$this->htmlLabels[$filter]}</label>
	   </div>
HTML;
		}

	}

	/**
	 *
	 */
	protected function resetQuery() {
		$this->query['where'] = [];
		if ( $this->hasCompany ) {
			$this->query['where'][] = [
				'key'     => 'company_parent',
				'compare' => 'EXISTS',
			];
		}
		$this->query['where'][] = [
			'key'   => 'post_status',
			'value' => 'publish',
		];
	}
}
