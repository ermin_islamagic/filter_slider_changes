<?php


namespace Financer\FilterSlider\Surface;


/**
 * Class Data
 * @package Financer\FilterSlider\Surface
 */
class Data extends \Surface\Data {
	protected $_validElementTypes = [
		Element::TH,
		Element::TD
	];

	/**
	 * @inheritDoc
	 */
	public function __construct( $value, $attributes = [], $elementType = Element::TD ) {
		parent::__construct( $value, $attributes, $elementType );
	}

	/**
	 * Render the data in an element
	 *
	 * @return string
	 */
	public function render() {
		$value = $this->_value;

		$attributes = $this->getAttributes();
		if ( ! isset( $attributes['class'] ) ) {
			$attributes['class'] = '';
		}
		$attributes['class'] = implode( ' ', array_merge( [ $this->_elementType ], array_filter( explode( ' ', $attributes['class'] ) ) ) );

		return $this->_renderElement( 'div', function () use ( $value ) {
			return $value;
		}, $attributes );
	}
}