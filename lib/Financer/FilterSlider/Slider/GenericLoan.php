<?php
/*
 * Generic loan slider
 */
namespace Financer\FilterSlider\Slider;


use Financer\FilterSlider\Abstracts\Slider;
use Financer\FilterSlider\Util;


/**
 * Class GenericLoan
 *
 * Generic loan slider
 *
 * @package Financer\FilterSlider\Slider
 */
class GenericLoan extends Slider {
	/**
	 * @var array
	 */
	protected $fields = [
		't.post_title as title',
		't.post_name as name',
		't.ID as ID',
		'ej_partner',
		'bad_history',
		'helgutbetalning AS weekend_payout',
		'favorite',
		'minalder',
		'loan_broker',
		'foretag',
		'url',
		'personal_loans',
		'average-rating.meta_value AS rating',
		'total-ratings.meta_value AS total_reviews',
		'loan_datasets.d.period_range_maximum',
		'loan_datasets.d.period_range_minimum',
		'loan_datasets.d.amount_range_minimum',
		'loan_datasets.d.amount_range_maximum',
		'MIN(loan_datasets.d.interest_rate) AS interest_rate',
		'loan_datasets.d.loan_restriction',
		'MIN(loan_datasets.d.highest_annual_interest_rate) AS highest_annual_interest_rate',
		'loan_datasets.d.custom_interest_rate',
		'loan_datasets.d.specific_affiliate_url',
		'MIN(loan_datasets.d.fee_flat) AS fee_flat',
		'MIN(loan_datasets.d.fee_percent) AS fee_percent',
		'MIN(loan_datasets.d.monthly_fee) AS monthly_fee',
		'loan_datasets.d.id AS pid',
		'loan_datasets.d.representative_example',
	];
	/**
	 * Pod setting storing slider comparison run count
	 * @var string
	 */
	protected $compareCounterSetting = 'loan_comparisons';

	/**
	 * @return string
	 */
	protected function preHeader(): string {
		$general_settings = pods( 'general_settings' );
		if ( $general_settings->field( 'charity_page' ) ) {
			return <<<HTML
    <a href="{$general_settings->field( 'charity_page.permalink' )}" rel="nofollow"><div class="charity-notice">{$this->htmlLabels['charity_notice']}</div></a>
HTML;
		}

		return '';
	}

	protected function afterHeader(): string {
		$slider_settings = pods( 'slider_settings' );
		if ( $slider_settings->field( 'loan_notice' ) ) {
			return <<<HTML
    <div class="loan-notice">{$slider_settings->field( 'loan_notice' )}</div>
HTML;
		}

		return '';
	}

	protected function loanQuiz(): string {
		$general_settings = pods( 'general_settings' );
		if ( $general_settings->field( 'loan_quiz_page' ) ) {
			return <<<HTML
<a href="{$general_settings->field( 'loan_quiz_page.permalink' )}" class="quiz-button">{$this->htmlLabels['loan_quiz_text']}</a>
HTML;
		}

		return '';
	}


	/**
	 * @inheritdoc
	 */
	protected function initQuery() {

		if(!empty(get_query_var( 'query' ))){
		$params = explode('/', get_query_var( 'query' ));
		$this->amount = $params[(array_search('param_amount', $params))+1];
		$this->period = $params[(array_search('param_period', $params))+1];
		
		}
		if ( empty( $this->amount ) ) {
			$this->amount = (int) $this->sliderSettings->field( 'default_loan_slider_amount' );
			
		}
		if ( empty( $this->period ) ) {
			$this->period = (int) $this->sliderSettings->field( 'default_loan_slider_period' );
		}

		parent::initQuery();
	}


	protected function labels() {
		parent::labels();
		$this->htmlLabels = [
			                    'lowest_interest_rate' => __( 'Lowest rate', 'fs' ),
		                    ] + $this->htmlLabels;
	}

	/**
	 * @return void
	 */
	protected function renderDifference() {
		global $wp_locale;
		$data    = $this->pod->data();
		$amounts = [];
		if ( ! empty( $data ) ) {
			$amounts = wp_list_pluck( $this->pod->data(), 'total_cost' );
			$amounts = array_map( 'floatval', $amounts );
			natsort( $amounts );
		}
		if ( empty( $amounts ) ) {
			$amounts[] = 0;
		}
		$min                                       = $amounts[0];
		$max                                       = end( $amounts );
		$old_decimal_point                         = $wp_locale->number_format['decimal_point'];
		$wp_locale->number_format['decimal_point'] = '.';
		$difference                                = Util::numberFormat( $max - $min );
		$wp_locale->number_format['decimal_point'] = $old_decimal_point;
		$symbol                                    = '&nbsp;' . __( 'usd', 'fs' );
		echo <<<HTML
<div class="block-circle">
    <p class="block-text">{$this->htmlLabels['difference_text']}</p>
    <p class="block-count"><span class="counter">{$difference}</span>$symbol</p>
</div>
HTML;

	}

	/**
	 *
	 */
	protected function renderCounterItems() {
		parent::renderCounterItems();
		static::renderLowestRate();
	}

	private function renderLowestRate() {
		$query           = $this->query;
		$query['select'] = 'MIN( interest_rate ) lowest_rate';
		unset( $query['orderby'] );
		$pod     = pods( $this->postType, $query );
		$percent = Util::numberFormat( $pod->field( 'lowest_rate' ) );
		echo <<<HTML
<div class="block-circle">
    <p class="block-text">{$this->htmlLabels['lowest_interest_rate']}</p>

    <p class="block-count"><span class="counter">{$percent}</span>%</p>
</div>
HTML;
	}


	protected function stepAge_dropdown() {

		parent::stepAge_dropdown();
		if($this->sliderSettings->field( 'enable_age_dropdown' )=='1'){
		/*age dropdown*/
		echo __( 'Age', 'fs' ) . ': 
		<select class="filter_nav" name="param_age_dropdown" id="param_age_dropdown">
			<option value="">' . __( 'select', 'fs' ) . '</option>
			<option value="18">18</option>
			<option value="19">19</option>
			<option value="20">20</option>
			<option value="21">21</option>
			<option value="22">22</option>
			<option value="23+">23+</option>
		</select>
		';
		}
	}


	protected function stepLimits_sort() {

		parent::stepLimits_sort();

		if ( $this->limitsEnabled || !$this->sortEnabled ) {
			echo <<<HTML
	    <div id="slider_options">
HTML;
			if ( $this->limitsEnabled ) {
				echo <<<HTML
	        <div class="itemCount">
	            {$this->htmlLabels['display']}:
	            <select class="filter_nav" name="param_limit">
HTML;
				foreach ( $this->limitList as $limit => $selected ) {
					$selected = selected( true, $selected );
					echo <<<HTML
		<option value="$limit"$selected>{$this->htmlLabels['display_' . $limit]}</option>
HTML;
				}
				echo <<<HTML
	            </select>
	        </div>
HTML;
			}
			if ( ! $this->sortEnabled ) {
echo '<div class="sort_type sort-text"><a>'. $this->htmlLabels['sort'] . ':</a></div>';
			foreach ( [ 'company-name', 'best-rating', 'interest-rate', 'total-cost' ] as $sortType ) {
				$sortTitle = ucfirst($sortType);
				$selected_current = ($this->sort==$sortType) ? ' current' : '' ;
				echo '<div class="sort_type sort_type_trigger '. $sortType . $selected_current . '" data-toggle="' . $sortType . '"><a href="javascript:void(0);" class="show-sort">' . __( $sortTitle, 'fs' ) . '</a></div>';
				}
			}


			echo <<<HTML
			</div>
HTML;
		}


	}


	protected function renderNewsletter(){

		parent::renderNewsletter();
		
		global $wpdb;

		 if ( 1 < $wpdb->blogid )
            $tb_prefix = $wpdb->base_prefix . $wpdb->blogid . '_';
        else
            $tb_prefix = $wpdb->base_prefix;

		$result_groupid = $wpdb->get_row( $wpdb->prepare( "SELECT group_id FROM {$tb_prefix}enewsletter_groups WHERE LOWER(group_name) = '%s'",  strtolower( $this->enewsletter_groupname ) ), "ARRAY_A");
		
		/*newsletter*/
			echo '<div class="enewsletter-slider">' . __( 'Found', 'fs' ) . ' <span class="found_total">' . $this->totalResults . '</span> ' . __( 'lenders. Would you like to be be notified (monthly) with new loan offers?', 'fs' ) . ' ' . do_shortcode( '[enewsletter_subscribe subscribe_to_groups="' . $result_groupid['group_id'] . '" show_groups="false" show_name=1]' ) . 
			'</div>';
		/*newsletter*/
	}

}
