<?php


namespace Financer\FilterSlider\Shortcode;


use Financer\FilterSlider\Abstracts\Shortcode;
use Financer\FilterSlider\Abstracts\Slider;
use Financer\FilterSlider\Plugin;
use Financer\FilterSlider\Slider\GenericLoan;

/**
 * Class Loan_Slider
 * @package Financer\FilterSlider\Shortcode
 */
class Loan_Slider extends Shortcode {

	/**
	 * @param array       $atts
	 *
	 * @param null|string $content
	 *
	 * @param string      $tag
	 * @param bool        $ajax
	 *
	 * @return mixed|string
	 */
	static function render( $atts, string $content = null, string $tag = null, $ajax = false ): string {
		$atts = shortcode_atts( [
			'title'          => '',
			'page'           => pods_field( 'general_settings', null, 'main_loan_page.ID' ),
			'default_amount' => 0,
			'default_period' => 0
		], $atts );
		ob_start();
		$deps = [
			'jquery-ui-widget',
			'jquery-ui-mouse',
			'jquery-ui-slider',
			'jquery-ui-tabs',
			'jquery-ui-dialog',
			'jquery-touch-punch',
			'jquery-effects-core',
		];
		foreach ( $deps as $script ) {
			wp_enqueue_script( $script );
		}
		$slider = new GenericLoan( [] );
		$slider->setAmount( (int) $atts['default_amount'] );
		$slider->setPeriod( (int) $atts['default_period'] );
		$mapOutput = $slider->renderJsMaps();

		wp_enqueue_script( 'dummy', Plugin::GetUri( 'js/dummy.js' ), [ 'jquery' ] );
		wp_add_inline_script( 'dummy', $mapOutput . "\n" . self::_renderJs( $slider, $atts ) );
		echo <<<HTML
		<div id="{$slider->getInstanceId()}_form">
HTML;
		$slider->setSteps( [ 'amount', 'period', 'submit' ] );
		$slider->runSteps();
		echo <<<HTML
		</div>
HTML;
		$slider::increaseInstanceCounter();

		return ob_get_clean();
	}

	/**
	 * @param Slider $slider
	 * @param array  $atts
	 *
	 * @return string
	 * @internal param string $instanceId
	 *
	 */
	private static function _renderJs( Slider $slider, array $atts ): string {
		$url         = get_the_permalink( $atts['page'] );
		$preloaderJs = $slider->renderPreloaderJs();

		$sliderInitJs = $slider->renderSliderInitJs();
		$sliderDataJs = $slider->renderJsData();

		return <<<JS
jQuery(function ($) {
    $sliderInitJs;
    $('#{$slider->getInstanceId()}_form .get_results').click(function (e) {
        $sliderDataJs;
        delete  data['action'];
        delete  data['name'];
        delete  data['email'];
        data = $.param(data).replace(/[&=]+/g, '/');
        window.location.href = '{$url}query/' + data;
        e.preventDefault();
        return false;
    }).closest('.widget').addClass('widget_filter_slider_loan');
});
$preloaderJs
JS;

	}

}