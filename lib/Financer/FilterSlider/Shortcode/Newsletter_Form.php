<?php

namespace Financer\FilterSlider\Shortcode;


use Financer\FilterSlider\Abstracts\Shortcode;

/**
 * Class Newsletter_Form
 * @package Financer\FilterSlider\Shortcode
 */
class Newsletter_Form extends Shortcode {
	public static $instance = 0;

	/**
	 * @param array       $atts
	 *
	 * @param null|string $content
	 *
	 * @param string      $tag
	 * @param bool        $ajax
	 *
	 * @return mixed|string
	 */
	static function render( $atts, string $content = null, string $tag = null, $ajax = false ): string {
		$emailPlaceholder = __( 'Enter your email', 'fs' );
		$namePlaceholder  = __( 'Enter your name', 'fs' );
		$button           = __( 'Subscribe', 'fs' );
		$instance         = self::$instance;
		self::$instance ++;
		$url          = get_site_url( null, '/wp-admin/admin-ajax.php' );
		$securityCode = wp_create_nonce( 'newsletter_form_nonce' );

		return <<<HTML
<div class="subscribe frm_form_fields" id="newsletter_form$instance">
    <label><p>{$atts['label']}</p><input type="text" placeholder="$namePlaceholder" class="name"/><input type="text" placeholder="$emailPlaceholder" class="email"/></label>
    <a href class="button medium lightblue">$button</a>

    <div class="status_bar">
        <p>
            <img class="loader"
                 src="/graph/loader.gif">
        </p>
    </div>
</div>
<script type="text/javascript">
    (function () {
        (function init() {
            if (typeof jQuery !== 'undefined') {

                jQuery(function ($) {
                    $('#newsletter_form$instance').find('a').click(function () {
                        var self = $(this);
                        jQuery.ajax({
                            url: '$url',
                            type: "POST",
                            data: {
                                action: "newsletter_form",
                                name: self.find('.name').val(),
                                email: self.find('.email').val(),
                                security: '$securityCode',
                                group: '{$atts['group']}'
                            },
                            beforeSend: function () {
                                self.siblings(".status_bar").fadeIn();
                            },
                            success: function (data) {
                                self.siblings(".status_bar").fadeOut();
                                jQuery(".table_cont").slideUp(1000, function () {
                                    self.siblings(".status").remove();
                                     jQuery('<p/>', {
                                            text: data.message,
                                            class: 'status'
                                        }).hide().addClass(data.error ? 'error' : 'success').appendTo(self.parent()).fadeIn().delay(10000).fadeOut();
                                });
                            },
                            error: function (msg) {
                                alert("<?php _e( 'Error - try another browser !:', 'fs' ); ?>" + msg);
                            }
                        });
                        return false;
                    })
                });

            } else {
                setTimeout(init, 50);
            }
        })
        ();

    })();
</script>
HTML;

	}

	/**
	 *
	 */
	public static function register() {
		parent::register();
		add_action(
			'wp_ajax_newsletter_form', [
				get_called_class(),
				'renderAjax',
			]
		);
		add_action(
			'wp_ajax_nopriv_newsletter_form', [
				get_called_class(),
				'renderAjax',
			]
		);
	}

	public static function renderAjax() {
		header( 'Content-Type: application/json' );
		if ( check_ajax_referer( 'newsletter_form_nonce', 'security', false ) ) {

			global $email_newsletter, $wpdb;
			$status = [ 'error' => false ];
			if ( false == empty( $_POST['email'] ) && is_email( $_POST['email'] ) ) {
				$_REQUEST['e_newsletter_email'] = sanitize_email( $_POST['email'] );
				$_REQUEST['e_newsletter_name']  = sanitize_text_field( $_POST['name'] );
				$group                          = sanitize_key( $_POST['group'] );
				if ( false == empty( $group ) ) {
					$member = $email_newsletter->get_member_by_email( $_POST['email'] );
					$group  = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM {$email_newsletter->tb_prefix}enewsletter_groups WHERE group_name = %s", $group ), "ARRAY_A" );
					if ( false == empty( $member ) ) {
						if ( empty( $member['unsubscribe_code'] ) ) {
							$status = [
								'error'   => true,
								'message' => __( 'Please check your email for a confirmation email before subscribing to another list',
									'email-newsletter' )
							];
						} else {
							$email_newsletter->add_members_to_groups( [ $member['member_id'] ], [ $group['group_id'] ] );
							$status = [ 'action' => 'new_subscribed', 'error' => false, 'message' => __( 'You have been successfully subscribed!', 'email-newsletter' ) ];
						}
					} else {
						$_REQUEST['e_newsletter_groups_id'] = [ $group['group_id'] ];
						$status                             = $email_newsletter->new_subscribe();
					}
				}
			} else if ( false == empty( $_POST['email'] ) && ! is_email( $_POST['email'] ) ) {
				$status = [ 'error' => true, 'message' => __( 'Invalid E-Mail Address!', 'fs' ) ];
			}
			echo json_encode( $status );
			wp_die();
		}
	}
}
